import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testData = [ Line(0,9,5,9)
           , Line(8,0,0,8)
           , Line(9,4,3,4)
           , Line(2,2,2,1)
           , Line(7,0,7,4)
           , Line(6,4,2,0)
           , Line(0,9,2,9)
           , Line(3,4,1,4)
           , Line(0,0,8,8)
           , Line(5,5,8,2)
           ]

testSmallBoard :: Assertion
testSmallBoard =
    assertEqual "Part two doesn't match" 12 (countOver1 testData 10 10)

main :: IO ()
main = defaultMain
  [ testCase "Part 2" testSmallBoard
  ]
