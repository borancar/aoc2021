module Lib
    ( Line (..)
    , boardAt
    , countOver1
    , pointOn
    ) where

data Line = Line (Int,Int,Int,Int)

pointOn :: Int -> Int -> Line -> Bool
pointOn x y l =
    let Line (p1x, p1y, p2x, p2y) = l
        dxc = x - p1x
        dyc = y - p1y
        dxl = p2x - p1x
        dyl = p2y - p1y
        cross = dxc * dyl - dyc * dxl
    in cross == 0
    && (dxl >= 0 && x >= p1x && x <= p2x || dxl < 0 && x >= p2x && x <= p1x)
    && (dyl >= 0 && y >= p1y && y <= p2y || dyl < 0 && y >= p2y && y <= p1y)

boardAt :: [Line] -> (Int,Int) -> Int
boardAt ls p =
    let (x, y) = p
    in sum $ map (\l -> if pointOn x y l then 1 else 0) ls

countOver1 :: [Line] -> Int -> Int -> Int
countOver1 ls sx sy =
    length $ filter (>1) $ map (boardAt ls) [(x, y) | x <- [0..sx], y <- [0..sy] ]

someFunc :: IO ()
someFunc = putStrLn "someFunc"
