module Main where

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

parsePoint :: String -> [Int]
parsePoint ss =
    map (read::String->Int) $ wordsWhen (== ',') ss

parseLine :: String -> Line
parseLine s =
    let [p1s, _, p2s] = words s
        [p1x, p1y] = parsePoint p1s
        [p2x, p2y] = parsePoint p2s
    in Line (p1x, p1y, p2x, p2y)

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let ls = map parseLine a
    putStrLn . show $ countOver1 ls 1000 1000
