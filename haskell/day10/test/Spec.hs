import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testInput :: [String]
testInput =
    [ "[({(<(())[]>[[{[]{<()<>>"
    , "[(()[<>])]({[<{<<[]>>("
    , "{([(<{}[<>[]}>{[]{[(<()>"
    , "(((({<>}<{<{<>}{[]{[]{}"
    , "[[<[([]))<([[{}[[()]]]"
    , "[{[{({}]{}}([{[{{{}}([]"
    , "{<[[]]>}<{[{[{[]{()[[[]"
    , "[<(<(<(<{}))><([]([]()"
    , "<{([([[(<>()){}]>(<<{{"
    , "<{([{{}}[<[[[<>{}]]]>[]]"
    ]

testPart1 :: Assertion
testPart1 = assertEqual "Part 1 doesn't match" 26397 (corrupted testInput)

testPart2 :: Assertion
testPart2 = assertEqual "Part 2 doesn't match" 288957 (completed testInput)

main :: IO ()
main = defaultMain
  [ testCase "Part 1" testPart1
  , testCase "Part 2" testPart2
  ]
