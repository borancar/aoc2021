module Main where

import Lib

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    putStrLn . show $ corrupted a
    putStrLn . show $ completed a
