module Lib
    ( complete
    , completed
    , corrupted
    ) where

import Data.Either
import Data.List

matching :: Char -> Char
matching '(' = ')'
matching '[' = ']'
matching '{' = '}'
matching '<' = '>'

completeR :: String -> [Char] -> Either String Char
completeR [] expect = Left expect
completeR (s:ss) expect
    | s `elem` ['(', '[', '{', '<'] = completeR ss ([matching s] ++ expect)
    | s `elem` [')', ']', '}', '>'] = if s == (head expect) then completeR ss (tail expect) else (Right s)

complete :: String -> Either String Char
complete ss =
    completeR ss []

scoreCorrupted :: Either String Char -> Int
scoreCorrupted (Right ')') = 3
scoreCorrupted (Right ']') = 57
scoreCorrupted (Right '}') = 1197
scoreCorrupted (Right '>') = 25137
scoreCorrupted _ = 0

corrupted :: [String] -> Int
corrupted ss =
    sum $ map (scoreCorrupted . complete) ss

scoreForCompletion :: Char -> Int
scoreForCompletion ')' = 1
scoreForCompletion ']' = 2
scoreForCompletion '}' = 3
scoreForCompletion '>' = 4

scoreCompletion :: Either String Char -> Int
scoreCompletion (Left comp) = foldl (\acc x -> 5*acc + scoreForCompletion x) 0 comp

completed :: [String] -> Int
completed ss =
    let scores = sort $ map scoreCompletion $ filter isLeft $ map complete ss
        len = length scores
    in scores !! (len `div` 2)
