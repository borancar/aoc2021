module Lib
    ( if'
    , foldX
    , foldY
    ) where

import qualified Data.Set as Set

if' :: Bool -> a -> a -> a
if' True x _ = x
if' False _ y = y

foldX :: Int -> Set.Set (Int, Int) -> Set.Set (Int, Int)
foldX fx =
    Set.map (\e -> let (x, y) = e in if' (x > fx) (2*fx - x, y) (x, y))

foldY :: Int -> Set.Set (Int, Int) -> Set.Set (Int, Int)
foldY fy =
    Set.map (\e -> let (x, y) = e in if' (y > fy) (x, 2*fy - y) (x, y))
