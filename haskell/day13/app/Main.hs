module Main where

import Data.Char
import Data.List
import qualified Data.Set as Set

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

execLine :: String -> Set.Set (Int, Int) -> Set.Set (Int, Int)
execLine "" = id
execLine ('f':'o':'l':'d':' ':'a':'l':'o':'n':'g':' ':'x':'=':sx) = foldX (read sx)
execLine ('f':'o':'l':'d':' ':'a':'l':'o':'n':'g':' ':'y':'=':sy) = foldY (read sy)
execLine ps = Set.insert (read sx, read sy)
              where sx = (wordsWhen (== ',') ps) !! 0
                    sy = (wordsWhen (== ',') ps) !! 1

outputAt :: Int -> Int -> Set.Set (Int, Int) -> Char
outputAt x y s
    | (x,y) `Set.member`s = '#'
    | otherwise           = '.'

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let transform = foldl1 (\x y -> y . x) $ map execLine a
        result = transform Set.empty
    putStr $ map (\(x,y) -> if' (x >= 50) '\n' (outputAt x y result)) $ [(x,y) | y <- [0..10], x <- [0..50]]
