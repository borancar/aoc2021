module Lib
    ( countElems
    , insert
    , maxElem
    , minElem
    , pairs
    , splitPair
    , stepPairs
    , templateToPairs
    ) where

import Data.List
import Data.Maybe
import qualified Data.Map as Map

pairs :: String -> [String]
pairs =
    foldr (zipWith (:)) (repeat []) . take 2 . tails

templateToPairs :: String -> Map.Map String Int
templateToPairs t =
    Map.fromListWith (+) (zip (pairs t) (repeat 1))

processInsert :: Map.Map String String -> (String, Int) -> [(String, Int)]
processInsert ins (p, f)
    | p `Map.member` ins =
        let c1 = head p
            c2 = (head . fromJust . Map.lookup p) ins
            c3 = p !! 1
        in [([c1, c2], f), ([c2, c3], f)]
    | otherwise        = [(p, f)]

stepPairs :: Map.Map String String -> Map.Map String Int -> Map.Map String Int
stepPairs ins m =
    let oldPairsF = Map.toList m
        newPairsF = concatMap (processInsert ins) oldPairsF
    in Map.fromListWith (+) newPairsF

splitPair :: (String, Int) -> [(Char, Int)]
splitPair (p, f) =
    let c1 = head p
        c2 = p !! 1
    in [(c1, f), (c2, f)]

countElems :: Map.Map String Int -> Map.Map Char Int
countElems m =
    Map.map ((`div` 2) . (+1)) $ Map.fromListWith (+) $ concatMap splitPair $ Map.toList m

minElem :: Map.Map Char Int -> (Char, Int)
minElem m =
    foldl1 (\a b -> if (snd a) < (snd b) then a else b) $ Map.toList m

maxElem :: Map.Map Char Int -> (Char, Int)
maxElem m =
    foldl1 (\a b -> if (snd a) > (snd b) then a else b) $ Map.toList m
