module Main where

import qualified Data.Map as Map

import Lib

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let template = head a
        ps = templateToPairs template
        ins = Map.fromList $ map (\p -> let w = words p in (head w, w !! 2)) $ (tail . tail) a
        dayNFunct = \n -> foldl1 (.) $ replicate n (stepPairs ins)
        elemsDay10 = countElems $ dayNFunct 10 ps
        elemsDay40 = countElems $ dayNFunct 40 ps
    print $ (snd (maxElem elemsDay10)) - (snd (minElem elemsDay10))
    print $ (snd (maxElem elemsDay40)) - (snd (minElem elemsDay40))
