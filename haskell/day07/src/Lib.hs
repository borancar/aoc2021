module Lib
    ( cost
    , distance1
    , distance2
    , maxA
    , minA
    , testInput
    ) where

testInput :: [Int]
testInput = [16,1,2,0,4,2,7,1,2,14]

minA :: [Int] -> Int
minA = foldl1 min

maxA :: [Int] -> Int
maxA = foldl1 max

distance1 :: Int -> Int -> Int
distance1 x y = abs (x - y)

distance2 :: Int -> Int -> Int
distance2 x y =
    let d = abs (x - y)
    in (d * (d+1)) `div` 2

cost :: (Int -> Int ->Int) -> [Int] -> [Int]
cost dist pos =
    [ sum (map (dist p) pos) | p <- [0..length pos] ]
