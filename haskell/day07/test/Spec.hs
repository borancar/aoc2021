import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testPart1 :: Assertion
testPart1 = assertEqual "Part 1 doesn't match" 37 (minA $ cost distance1 testInput)

testPart2 :: Assertion
testPart2 = assertEqual "Part 2 doesn't match" 168 (minA $ cost distance2 testInput)

main :: IO ()
main = defaultMain
  [ testCase "Part 1" testPart1
  , testCase "Part 2" testPart2
  ]
