module Lib
    ( diff
    , solve
    , sums
    , windows
    ) where

import Data.List (tails)

windows :: (Eq a) => Int -> [a] -> [[a]]
windows w = foldr (zipWith (:)) (repeat []) . take w . tails

sums :: (Num a, Eq a) => Int -> [a] -> [a]
sums w = map sum . windows w

diff :: (Num a, Eq a) => [a] -> [a]
diff xs = zipWith (-) (tail xs) xs

solve :: (Num a, Ord a, Eq a) => [a] -> a
solve = sum . map (\x -> if (x > 0) then 1 else 0) . diff . sums 3
