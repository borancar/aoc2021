module Main where

import Lib

main :: IO ()
main = do
  a <- lines <$> readFile "input.txt"
  let depths = map (read::String->Int) a in
      putStrLn . show $ solve depths
