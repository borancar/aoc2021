import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testWindows3 :: Assertion
testWindows3 =
  assertEqual "windows 3 doesn't match" ([[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 7]]) (windows 3 [1, 2, 3, 4, 5, 6, 7])

testSums3 :: Assertion
testSums3 =
  assertEqual "sums 3 doesn't match" ([6, 9, 12, 15, 18]) (sums 3 [1, 2, 3, 4, 5, 6, 7])

testSolve :: Assertion
testSolve =
  assertEqual "solution doesn't match" 5 (solve [199, 200, 208, 210, 200, 207, 240, 269, 260, 263])

main :: IO ()
main = defaultMain
  [ testCase "test-windows-3" testWindows3
  , testCase "test-sums-3" testSums3
  , testCase "test-solve" testSolve
  ]
