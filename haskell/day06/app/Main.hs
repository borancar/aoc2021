module Main where

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let inputData = map (read::String->Int) $ wordsWhen (==',') (a !! 0)
    putStrLn . show $ howMany inputData 256
