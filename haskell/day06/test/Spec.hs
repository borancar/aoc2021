import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testData = [3,4,3,1,2]

testPart1Day18 :: Assertion
testPart1Day18 = assertEqual "Day 18 doesn't match" 26 (howMany testData 18)

testPart1Day80 :: Assertion
testPart1Day80 = assertEqual "Day 80 doesn't match" 5934 (howMany testData 80)

testPart1Day256 :: Assertion
testPart1Day256 = assertEqual "Day 80 doesn't match" 26984457539 (howMany testData 256)

main :: IO ()
main = defaultMain
  [ testCase "Test day 18" testPart1Day18
  , testCase "Test day 80" testPart1Day80
  , testCase "Test day 256" testPart1Day256
  ]
