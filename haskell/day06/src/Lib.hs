module Lib
    ( howMany
    ) where

import Data.Function (fix)
import Debug.Trace

increase :: [Int] -> (Int -> Int) -> Int -> Int
increase initial f 0   = 0
increase initial f day = (length $ filter (== day) initial)
                       + (if day >= 7 then f (day - 7) else 0)
                       + (if day >= 9 then f (day - 9) else 0)

memoize :: (Int -> Int) -> (Int -> Int)
memoize f = (map f [0..] !!)

increaseMemo :: [Int] -> Int -> Int
increaseMemo initial = fix (memoize . (increase initial))

howMany :: [Int] -> Int -> Int
howMany initial day = foldl (+) (length initial) [(increaseMemo initial) d | d <- [0..day-1]]
