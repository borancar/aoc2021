import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

import qualified Data.Set as Set

testInput5353 :: Line
testInput5353 =
    Line { input = [Set.fromList "acedgfb", Set.fromList "cdfbe", Set.fromList "gcdfa", Set.fromList "fbcad", Set.fromList "dab", Set.fromList "cefabd", Set.fromList "cdfgeb", Set.fromList "eafb", Set.fromList "cagedb", Set.fromList "ab"]
         , output = [Set.fromList "cdfeb", Set.fromList "fcadb", Set.fromList "cdfeb", Set.fromList "cdbaf"]
         }

testInput8394 :: Line
testInput8394 =
    Line { input = [Set.fromList "be", Set.fromList "cfbegad", Set.fromList "cbdgef", Set.fromList "fgaecd", Set.fromList "cgeb", Set.fromList "fdcge", Set.fromList "agebfd", Set.fromList "fecdb", Set.fromList "fabcd", Set.fromList "edb"]
         , output = [Set.fromList "fdgacbe", Set.fromList "cefdb", Set.fromList "cefbgd", Set.fromList "gcbe"]
         }

test5353 :: Assertion
test5353 = assertEqual "5353 doesn't match" 5353 (toInt $ transform (deduce (input testInput5353)) (output testInput5353))

test8394 :: Assertion
test8394 = assertEqual "8394 doesn't match" 8394 (toInt $ transform (deduce (input testInput8394)) (output testInput8394))

main :: IO ()
main = defaultMain
  [ testCase "5353" test5353
  , testCase "8394" test8394
  ]
