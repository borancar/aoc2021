module Main where

import qualified Data.Set as Set

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

parseLine :: String -> Line
parseLine l =
    let inoutParts = wordsWhen (== '|') l
        inputs = words (inoutParts !! 0)
        outputs = words (inoutParts !! 1)
    in Line { input = map Set.fromList inputs, output = map Set.fromList outputs }

parseLines :: [String] -> [Line]
parseLines = map parseLine

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    print . sum $ map count1478 $ parseLines a
    print . sum $ map readOutput $ parseLines a
