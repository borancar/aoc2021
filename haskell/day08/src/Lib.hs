module Lib
    ( Line (..)
    , count1478
    , deduce
    , readOutput
    , toInt
    , transform
    ) where

import Data.Maybe
import qualified Data.Set as Set
import qualified Data.List as List

data Line = Line { input  :: [Set.Set Char]
                 , output :: [Set.Set Char]
                 }
                 deriving (Show, Eq)

deduceBySize :: Int -> [Set.Set Char] -> [Set.Set Char]
deduceBySize sz =
    filter (\s -> Set.size s == sz)

count1478 :: Line -> Int
count1478 l =
    length $ filter (\x -> length x `elem` [2,4,3,7]) $ output l

deduce :: [Set.Set Char] -> [Set.Set Char]
deduce sets =
    let deduce069 = deduceBySize 6 sets
        deduce1   = head $ deduceBySize 2 sets
        deduce235 = deduceBySize 5 sets
        deduce4   = head $ deduceBySize 4 sets
        deduce7   = head $ deduceBySize 3 sets
        deduce8   = head $ deduceBySize 7 sets
        ([deduce3], deduce25) = List.partition (\s -> deduce1 `Set.isSubsetOf` s) deduce235
        (deduce09, [deduce6]) = List.partition (\s -> deduce1 `Set.isSubsetOf` s) deduce069
        ([deduce9], [deduce0]) = List.partition (\s -> deduce4 `Set.isSubsetOf` s) deduce09
        ([deduce5], [deduce2]) = List.partition (\s -> s `Set.isSubsetOf` deduce6) deduce25
    in [deduce0, deduce1, deduce2, deduce3, deduce4, deduce5, deduce6, deduce7, deduce8, deduce9]

transform :: [Set.Set Char] -> [Set.Set Char] -> [Int]
transform tr =
    map (\i -> fromJust (List.elemIndex i tr))

toInt :: [Int] -> Int
toInt = foldl (\acc x -> 10*acc + x) 0

readOutput :: Line -> Int
readOutput l =
    let tr = deduce (input l)
    in toInt $ transform tr (output l)
