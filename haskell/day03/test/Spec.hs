import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testData :: [[Int]]
testData = [ [0, 0, 1, 0, 0]
           , [1, 1, 1, 1, 0]
           , [1, 0, 1, 1, 0]
           , [1, 0, 1, 1, 1]
           , [1, 0, 1, 0, 1]
           , [0, 1, 1, 1, 1]
           , [0, 0, 1, 1, 1]
           , [1, 1, 1, 0, 0]
           , [1, 0, 0, 0, 0]
           , [1, 1, 0, 0, 1]
           , [0, 0, 0, 1, 0]
           , [0, 1, 0, 1, 0]
           ]

testGamma :: Assertion
testGamma =
    assertEqual "Gamma incorrect" 22 ( toNum . gamma . accum $ map mapInput $ testData )

testEpsilon :: Assertion
testEpsilon =
    assertEqual "Epsilon incorrect" 9 ( toNum . epsilon . accum $ map mapInput $ testData )

testO2 :: Assertion
testO2 =
    let work = map mapInput $ testData
        acc = accum work
    in assertEqual "O2 incorrect" 23 ( toNum $ solve stepO2 0 work )

testCO2 :: Assertion
testCO2 =
    let work = map mapInput $ testData
        acc = accum work
    in assertEqual "CO2 incorrect" 10 ( toNum $ solve stepCO2 0 work )

main :: IO ()
main = defaultMain
  [ testCase "gamma" testGamma
  , testCase "epsilon" testEpsilon
  , testCase "O2" testO2
  , testCase "CO2" testCO2
  ]
