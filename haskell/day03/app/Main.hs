module Main where

import Lib

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let input = map parseNumber a
        work = map mapInput input
        acc = accum work
        gam = toNum $ gamma acc
        eps = toNum $ epsilon acc
        o2 = toNum . unmapInput $ solve stepO2 0 work
        co2 = toNum . unmapInput $ solve stepCO2 0 work
    putStrLn $ show (gam * eps)
    putStrLn $ show (o2 * co2)
