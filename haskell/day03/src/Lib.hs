module Lib
    ( accum
    , epsilon
    , gamma
    , mapInput
    , parseNumber
    , solve
    , stepO2
    , stepCO2
    , toNum
    , unmapInput
    ) where

parseNumber :: String -> [Int]
parseNumber = map (\x -> if x == '1' then 1 else 0)

mapInput :: [Int] -> [Int]
mapInput = map (\x -> if x > 0 then 1 else -1)

unmapInput :: [Int] -> [Int]
unmapInput = map (\x -> if x > 0 then 1 else 0)

accum :: [[Int]] -> [Int]
accum = foldl1 (zipWith (+))

gamma :: [Int] -> [Int]
gamma = map (\x -> if x > 0 then 1 else 0)

epsilon :: [Int] -> [Int]
epsilon = map (\x -> if x < 0 then 1 else 0)

toNum :: [Int] -> Int
toNum = foldl1 (\a x -> 2*a + x)

stepO2 :: Int -> [[Int]] -> [[Int]]
stepO2 i work =
    let acc = accum work
    in filter (\n -> if (acc !! i >= 0) then n !! i == 1 else n !! i == -1) work

stepCO2 :: Int -> [[Int]] -> [[Int]]
stepCO2 i work =
    let acc = accum work
    in filter (\n -> if (acc !! i >= 0) then n !! i == -1 else n !! i == 1) work

solve :: (Int -> [[Int]] -> [[Int]]) -> Int -> [[Int]] -> [Int]
solve step i work
    | length work == 1 = unmapInput $ work !! 0
    | otherwise        = solve step (i+1) (step i work)
