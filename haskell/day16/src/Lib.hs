module Lib
    ( Packet (..)
    , bin2Dec
    , executePacket
    , hex2Bin
    , parseLiteral
    , parsePacket
    , parseString
    ) where

import Data.List

data Packet = SumOp [Packet]
            | ProdOp [Packet]
            | MinOp [Packet]
            | MaxOp [Packet]
            | Lit Int
            | GtOp [Packet]
            | LtOp [Packet]
            | EqOp [Packet]
            deriving (Show, Eq)

bin2Dec :: [Int] -> Int
bin2Dec = foldl1 (\acc x -> 2*acc + x)

parseLiteral :: [Int] -> ([Int], Int)
parseLiteral (0:bs) = (take 4 bs, 5)
parseLiteral (1:bs) =
    let (g, rest)  = splitAt 4 bs
        (ngs, len) = parseLiteral rest
    in (g ++ ngs, 5 + len)

parseSubLen :: [Int] -> Int -> Int -> [Packet] -> ([Packet], Int)
parseSubLen bs rem totalLen ps
    | rem == 0  = (ps, totalLen)
    | otherwise = 
        let (p, l)    = parsePacket bs
            (_, rest) = splitAt l bs
        in  parseSubLen rest (rem - l) (totalLen + l) (ps ++ [p])

parseSubN :: [Int] -> Int -> Int -> [Packet] -> ([Packet], Int)
parseSubN bs rem totalLen ps
    | rem == 0  = (ps, totalLen)
    | otherwise = 
        let (p, l)    = parsePacket bs
            (_, rest) = splitAt l bs
        in  parseSubN rest (rem - 1) (totalLen + l) (ps ++ [p])

parseOp :: [Int] -> ([Packet], Int)
parseOp (0:bs) =
    let (lbits, rest) = splitAt 15 bs
        len = bin2Dec lbits
        (subs, plen) = parseSubLen rest len 0 []
    in  (subs, 1 + 15 + plen)
parseOp (1:bs) =
    let (nbits, rest) = splitAt 11 bs
        npackets = bin2Dec nbits
        (subs, plen) = parseSubN rest npackets 0 []
    in  (subs, 1 + 11 + plen)

parsePacket :: [Int] -> (Packet, Int)
parsePacket bs =
    let (versionBits, type_rest) = splitAt 3 bs
        (typeBits, rest) = splitAt 3 type_rest
        version = bin2Dec versionBits
        type_ = bin2Dec typeBits
    in  case type_ of
      0 -> let (subs, len) = parseOp rest
           in  (SumOp subs, 6 + len)
      1 -> let (subs, len) = parseOp rest
           in  (ProdOp subs, 6 + len)
      2 -> let (subs, len) = parseOp rest
           in  (MinOp subs, 6 + len)
      3 -> let (subs, len) = parseOp rest
           in  (MaxOp subs, 6 + len)
      4 -> let (bits, len) = parseLiteral rest
           in  (Lit (bin2Dec bits), 6 + len)
      5 -> let (subs, len) = parseOp rest
           in  (GtOp subs, 6 + len)
      6 -> let (subs, len) = parseOp rest
           in  (LtOp subs, 6 + len)
      7 -> let (subs, len) = parseOp rest
           in  (EqOp subs, 6 + len)

hex2Bin :: Char -> [Int]
hex2Bin '0' = [0,0,0,0]
hex2Bin '1' = [0,0,0,1]
hex2Bin '2' = [0,0,1,0]
hex2Bin '3' = [0,0,1,1]
hex2Bin '4' = [0,1,0,0]
hex2Bin '5' = [0,1,0,1]
hex2Bin '6' = [0,1,1,0]
hex2Bin '7' = [0,1,1,1]
hex2Bin '8' = [1,0,0,0]
hex2Bin '9' = [1,0,0,1]
hex2Bin 'A' = [1,0,1,0]
hex2Bin 'B' = [1,0,1,1]
hex2Bin 'C' = [1,1,0,0]
hex2Bin 'D' = [1,1,0,1]
hex2Bin 'E' = [1,1,1,0]
hex2Bin 'F' = [1,1,1,1]

executePacket :: Packet -> Int
executePacket (Lit v) = v
executePacket (SumOp subs) = foldl1 (+) $ map executePacket subs
executePacket (ProdOp subs) = foldl1 (*) $ map executePacket subs
executePacket (MinOp subs) = foldl1 min $ map executePacket subs
executePacket (MaxOp subs) = foldl1 max $ map executePacket subs
executePacket (GtOp subs) = if a > b  then 1 else 0 where a = executePacket (subs !! 0); b = executePacket (subs !! 1)
executePacket (LtOp subs) = if a < b  then 1 else 0 where a = executePacket (subs !! 0); b = executePacket (subs !! 1)
executePacket (EqOp subs) = if a == b then 1 else 0 where a = executePacket (subs !! 0); b = executePacket (subs !! 1)

parseString :: String -> [Int]
parseString =
    concatMap hex2Bin
