module Main where

import Lib

main :: IO ()
main = do
    a <- head . lines <$> readFile "input.txt"
    let (packet, _) = (parsePacket . parseString) a
    print $ executePacket packet
