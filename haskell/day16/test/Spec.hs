import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testParseLiteral :: Assertion
testParseLiteral =
    assertEqual "Parsed incorrectly" (Lit 2021) (fst $ parsePacket $ parseString "D2FE28")

testParseOplen2Lits :: Assertion
testParseOplen2Lits =
    assertEqual "Parsed incorrectly" (LtOp [Lit 10, Lit 20]) (fst $ parsePacket $ parseString "38006F45291200")

testParseOpn3Lits :: Assertion
testParseOpn3Lits =
    assertEqual "Parsed incorrectly" (MaxOp [Lit 1, Lit 2, Lit 3]) (fst $ parsePacket $ parseString "EE00D40C823060")

testSingleNested :: Assertion
testSingleNested =
    assertEqual "Parsed incorrectly" (MinOp [MinOp [MinOp [Lit 15]]]) (fst $ parsePacket $ parseString "8A004A801A8002F478")

testDoubleNested :: Assertion
testDoubleNested =
    assertEqual "Parsed incorrectly" (SumOp [SumOp [Lit 10, Lit 11], SumOp [Lit 12, Lit 13]]) (fst $ parsePacket $ parseString "620080001611562C8802118E34")

testDoubleNested2 :: Assertion
testDoubleNested2 =
    assertEqual "Parsed incorrectly" (SumOp [SumOp [Lit 10, Lit 11], SumOp [Lit 12, Lit 13]]) (fst $ parsePacket $ parseString "C0015000016115A2E0802F182340")

main :: IO ()
main = defaultMain
  [ testCase "Parse Literal" testParseLiteral
  , testCase "Parse Operator with 2 Literals" testParseOplen2Lits
  , testCase "Parse Operator with 3 Literals" testParseOpn3Lits
  , testCase "Parse Operator with single nested" testSingleNested
  , testCase "Parse Operator with double nested" testDoubleNested
  , testCase "Parse Operator with double nested 2" testDoubleNested2
  ]
