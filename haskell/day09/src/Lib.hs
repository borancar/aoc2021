module Lib
    ( Point (..)
    , basin
    , enumerate
    , filterNext
    , lowPoints
    , neighbours
    , pointValue
    , pointValues
    , newMap
    , top3Basins
    , unwrap
    , unwrap1
    ) where

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

data Point = Point (Int, Int) deriving (Show, Eq, Ord)

instance Num Point where
    Point (a, b) + Point (c, d) = Point (a+c, b+d)
    Point (a, b) - Point (c, d) = Point (a-c, b-d)
    Point (a, b) * Point (c, d) = Point (a*c, b*d)
    abs (Point (a, b)) = Point (abs a, abs b)
    signum (Point (a, b)) = Point (signum a, signum b)
    fromInteger i = Point (fromInteger i, fromInteger i)

enumerate :: [[Int]] -> [(Int, Int, Int)]
enumerate m = concat $ map (\yr -> zip3 [0..] (repeat (fst yr)) (snd yr)) (zip [0..] m)

pointValue :: (Int, Int, Int) -> (Point, Int)
pointValue (x, y, v) = (Point(x, y), v)

pointValues :: [[Int]] -> [(Point, Int)]
pointValues m = map pointValue $ enumerate m

newMap :: [[Int]] -> Map.Map Point Int
newMap m = Map.fromList $ pointValues m

unwrap1 :: Maybe a -> [a]
unwrap1 Nothing  = []
unwrap1 (Just (n)) = [n]

unwrap :: [Maybe a] -> [a]
unwrap l = concat $ map unwrap1 l

neighbours :: Map.Map Point Int -> Point -> [(Point, Int)]
neighbours m p =
    let points = zipWith (+) [Point(-1,0),Point(0,-1),Point(1,0),Point(0,1)] (repeat p)
        values = map (\p -> Map.lookup p m) points
    in unwrap $ map sequence (zip points values)

lowPoints :: [[Int]] -> [(Point,Int)]
lowPoints mat =
    let m = newMap mat
        pvs = pointValues mat
    in filter (\e1 -> let (p1, v1) = e1 in all (\e2 -> let (p2, v2) = e2 in v1 < v2) (neighbours m p1)) pvs

filterNext :: (Point, Int) -> [(Point, Int)] -> [(Point, Int)]
filterNext pv nps =
    let (p, v) = pv
    in filter (\e -> let (pn, vn) = e in vn > v && vn /= 9) nps

basin :: Map.Map Point Int -> (Point,Int) -> Set.Set Point
basin m pv =
    let p = fst pv
        next = neighbours m p
    in Set.fromList [p] `Set.union` (Set.unions $ map (basin m) $ filterNext pv next)

top3Basins :: [[Int]] -> [Int]
top3Basins mat =
    let lows = lowPoints mat
        m = newMap mat
        basins = map (basin m) lows
        sizes = map (Set.size) basins
    in take 3 $ reverse $ List.sort $ sizes
