module Main where

import Lib

parseMat :: [String] -> [[Int]]
parseMat =
    (map . map) (read . pure :: Char -> Int)

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    putStrLn . show $ foldl1 (*) $ top3Basins $ parseMat a
