import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testInput :: [[Int]]
testInput =
    [ [2, 1, 9, 9, 9, 4, 3, 2, 1, 0]
    , [3, 9, 8, 7, 8, 9, 4, 9, 2, 1]
    , [9, 8, 5, 6, 7, 8, 9, 8, 9, 2]
    , [8, 7, 6, 7, 8, 9, 6, 7, 8, 9]
    , [9, 8, 9, 9, 9, 6, 5, 6, 7, 8]
    ]

testPart1 :: Assertion
testPart1 =
    assertEqual "Part1 doesn't match" 15 (sum $ map ((+1) . snd) $ lowPoints testInput)

testPart2 :: Assertion
testPart2 =
    assertEqual "Part2 doesn't match" 1134 (foldl1 (*) $ top3Basins testInput)

main :: IO ()
main = defaultMain
  [ testCase "Part 1 test" testPart1
  , testCase "Part 2 test" testPart2
  ]
