module Main where

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

parseBoards :: [[String]] -> [[[String]]]
parseBoards =
    (map . map) words

extractBlocks :: [String] -> [[String]] -> [[String]]
extractBlocks [] accum = accum
extractBlocks unps accum =
    let (nps, nunps) = extractBlock unps
    in extractBlocks nunps (accum ++ [nps])

extractBlock :: [String] -> ([String], [String])
extractBlock ls =
    span (/= "") $ dropWhile (== "") ls

main :: IO ()
main = do
  a <- lines <$> readFile "input.txt"
  let numbers = map (read::String->Int) $ wordsWhen (==',') $ head a
      boards = map newBoard $ (map . map . map) (read::String->Int) $ parseBoards $ extractBlocks (tail a) []
      ends = findBingo boards numbers
      firstBingo = fst $ minTurns ends
      lastBingo = fst $ maxTurns ends
  putStrLn . show $ score firstBingo * lastN firstBingo
  putStrLn . show $ score lastBingo * lastN lastBingo
