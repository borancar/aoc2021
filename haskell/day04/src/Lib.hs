module Lib
    ( Board (..)
    , col
    , findBingo
    , maxTurns
    , minTurns
    , newBoard
    , playBoard
    , row
    , score
    , untilBingo
    , bingo
    ) where

import Data.Maybe

data Board = Board { rows  :: [[Int]]
                   , cols  :: [[Int]]
                   , lastN :: Int
                   } deriving (Show)

row :: Int -> [[Int]] -> [Int]
row r b = b !! r

col :: Int -> [[Int]] -> [Int]
col c = map (\e -> e !! c)

newBoard :: [[Int]] -> Board
newBoard m =
    Board { rows  = [ row r m | r <- [0..4] ]
          , cols  = [ col c m | c <- [0..4] ]
          , lastN = 0
          }

playBoard :: Board -> Int -> Board
b `playBoard` n =
    Board { rows  = map (filter ((/=) n)) (rows b)
          , cols  = map (filter ((/=) n)) (cols b)
          , lastN = n
          }

bingo :: Board -> Bool
bingo b = any ((==0) . length) (rows b) || any ((==0) . length) (cols b)

untilBingo :: Board -> [Int] -> Int -> (Board, Int)
untilBingo b (n:ns) t
    | bingo b   = (b, t)
    | otherwise = untilBingo (b `playBoard` n) ns (t + 1)

findBingo :: [Board] -> [Int] -> [(Board, Int)]
findBingo bs ns =
    map (\b -> untilBingo b ns 0) bs

score :: Board -> Int
score b =
    sum $ map sum $ rows b

minTurns :: [(Board, Int)] -> (Board, Int)
minTurns [(b, t)] = (b, t)
minTurns ((b1, t1):(b2, t2):bts)
    | t1 < t2  = minTurns ((b1, t1):bts)
    | t2 < t1  = minTurns ((b2, t2):bts)
    | t1 == t2 = minTurns ((b2, t2):bts)

maxTurns :: [(Board, Int)] -> (Board, Int)
maxTurns [(b, t)] = (b, t)
maxTurns ((b1, t1):(b2, t2):bts)
    | t1 > t2  = maxTurns ((b1, t1):bts)
    | t2 > t1  = maxTurns ((b2, t2):bts)
    | t1 == t2 = maxTurns ((b2, t2):bts)
