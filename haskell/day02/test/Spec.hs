import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testNavigation :: Assertion
testNavigation =
    assertEqual "Factor doesn't match" 900 ( navigate [Forward 5, Down 5, Forward 8, Up 3, Down 8, Forward 2] )

main :: IO ()
main = defaultMain
  [ testCase "navigation" testNavigation
  ]
