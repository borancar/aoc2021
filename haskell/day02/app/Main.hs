module Main where

import Lib

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let cmds = map parseCmd a in
        putStrLn . show $ navigate cmds
