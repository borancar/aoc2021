module Lib
    ( Command (..)
    , SubPosition (..)
    , moveSub
    , parseCmd
    , navigate
    ) where

data SubPosition = SubPosition { aim :: Int
                               , horiz :: Int
                               , vert :: Int
                               } deriving (Show)

data Command = Forward Int
             | Up Int
             | Down Int
             deriving (Show)

parseCmd :: String -> Command
parseCmd s =
    let split = words s
        dir = head split
        amount = (read::String->Int) . head . tail $ split
    in actualParseCmd dir amount

actualParseCmd :: String -> Int -> Command
actualParseCmd "forward" d = Forward d
actualParseCmd "up" a = Up a
actualParseCmd "down" a = Down a

moveSub :: SubPosition -> Command -> SubPosition
sub `moveSub` Forward d = sub { horiz = horiz sub + d, vert = vert sub + aim sub * d }
sub `moveSub` Up a = sub { aim = aim sub - a }
sub `moveSub` Down a = sub { aim = aim sub + a }

navigate :: [Command] -> Int
navigate cmds =
    let initial = SubPosition { aim = 0, horiz = 0, vert = 0 }
        position = foldl (moveSub) initial cmds
    in (horiz position) * (vert position)
