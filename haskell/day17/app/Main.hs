module Main where

import Data.List

import Lib

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

parseRange :: String -> (Int, Int)
parseRange rs =
    let parts = wordsWhen (== '.') rs
        (from, to) = splitAt 1 parts
    in (read (head from), read (head to))

parseX :: String -> (Int, Int)
parseX (' ':'x':'=':rs) = parseRange rs
parseX s = error ("Unknown " ++ s)

parseY :: String -> (Int, Int)
parseY (' ':'y':'=':rs) = parseRange rs
parseY s = error ("Unknown " ++ s)

parseTarget :: String -> ((Int, Int), (Int, Int))
parseTarget ('t':'a':'r':'g':'e':'t':' ':'a':'r':'e':'a':':':ss) =
    let [xs, ys] = wordsWhen (== ',') ss
    in (parseX xs, parseY ys)
parseTarget s = error ("Unknown " ++ s)

main :: IO ()
main = do
    a <- head . lines <$> readFile "input.txt"
    let target = parseTarget a
    print $ maxHeight target
    print $ viable target
