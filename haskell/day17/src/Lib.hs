module Lib
    ( drag
    , fire
    , maxHeight
    , viable
    ) where

import Data.Maybe
import Debug.Trace

drag :: Int -> Int
drag x
    | x > 0     = x - 1
    | x < 0     = x + 1
    | otherwise = 0

fire :: (Int, Int) -> (Int, Int) -> Int -> ((Int, Int), (Int, Int)) -> Maybe Int
fire (cx, cy) (vx, vy) maxy rect
    | (cx >= sx && cx <= ex) && (cy >= sy && cy <= ey) = Just maxy
    | (cx >= ex) || (cy <= sy)                         = Nothing
    | otherwise =
        fire (cx + vx, cy + vy) (drag vx, vy - 1) (max (cy + vy) maxy) rect
    where ((sx, ex), (sy, ey)) = rect

maxHeight :: ((Int, Int), (Int, Int)) -> Int
maxHeight target =
    let ((sx, ex), (sy, ey)) = target
        min_vx = round ((sqrt (1 + 8 * fromIntegral sx) - 1) / 2)
        max_vx = ex
        min_vy = sy
        max_vy = -sy - 1
    in fromJust $ fire (0, 0) (min_vx+1, max_vy) 0 target

viable :: ((Int, Int), (Int, Int)) -> Int
viable target =
    let ((sx, ex), (sy, ey)) = target
        min_vx = round ((sqrt (1 + 8 * fromIntegral sx) - 1) / 2)
        max_vx = ex
        min_vy = sy
        max_vy = -sy - 1
    in length $ catMaybes $ [fire (0, 0) (vx, vy) 0 target | vx <- [min_vx..max_vx], vy <- [min_vy..max_vy]]
