module Main where

import Lib
import Data.Char

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
              where (w, s'') = break p s'

parseCave :: String -> Cave
parseCave "start" = Start
parseCave "end"   = End
parseCave s
    | all isLower s = Small s
    | all isUpper s = Big s
    | otherwise     = error "Invalid cave name"

parseEdge :: String -> Edge
parseEdge line =
  Edge (parseCave left,parseCave right)
  where [left,right] = wordsWhen (== '-') line

main :: IO ()
main = do
    a <- lines <$> readFile "input.txt"
    let edges = map parseEdge a
        adj = toAdj edges
        part1 = pathfind1 adj Start []
        part2 = pathfind2 adj Start [] Nothing
    (print . length) part1
    (print . length) part2
