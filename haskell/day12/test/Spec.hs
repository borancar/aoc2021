import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit

import Lib

testInput :: [Edge]
testInput =
    [ Edge(Start, Big "A")
    , Edge(Start, Small "b")
    , Edge(Big "A", Small "c")
    , Edge(Big "A", Small "b")
    , Edge(Small "b", Small "d")
    , Edge(Big "A", End)
    , Edge(Small "b", End)
    ]

testPart1 :: Assertion
testPart1 = assertEqual "Part 1 doesn't match" 10 (length $ pathfind1 next Start [])
  where next = toAdj testInput

testPart2 :: Assertion
testPart2 = assertEqual "Part 2 doesn't match" 36 (length $ pathfind2 next Start [] Nothing)
  where next = toAdj testInput

main :: IO ()
main = defaultMain
  [ testCase "Part 1" testPart1
  , testCase "Part 2" testPart2
  ]
