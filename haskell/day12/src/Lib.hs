module Lib
    ( Cave (..)
    , Edge (..)
    , inserts
    , isSmall
    , nexts
    , pathfind1
    , pathfind2
    , populateAdj
    , toAdj
    ) where

import Data.Maybe
import qualified Data.Map as Map
import qualified Data.Set as Set

data Cave = Start | End | Big String | Small String deriving (Show, Eq)

instance Ord Cave where
  compare Start     Start     = EQ
  compare Start     _         = LT
  compare _         Start     = GT
  compare End       End       = EQ
  compare _         End       = LT
  compare End       _         = GT
  compare (Small a) (Small b) = compare a b
  compare (Big a)   (Big b)   = compare a b
  compare (Small _) (Big _)   = LT
  compare (Big _)   (Small _) = GT

isSmall :: Cave -> Bool
isSmall (Small _) = True
isSmall _ = False

newtype Edge = Edge (Cave, Cave) deriving (Eq, Show)

inserts :: Edge -> [Map.Map Cave (Set.Set Cave) -> Map.Map Cave (Set.Set Cave)]
inserts (Edge (Start, c)) = [ Map.insertWith Set.union Start (Set.singleton c)   ]
inserts (Edge (c, Start)) = [ Map.insertWith Set.union Start (Set.singleton c)   ]
inserts (Edge (End, c))   = [ Map.insertWith Set.union c     (Set.singleton End) ]
inserts (Edge (c, End))   = [ Map.insertWith Set.union c     (Set.singleton End) ]
inserts (Edge (c1, c2))   = [ Map.insertWith Set.union c1    (Set.singleton c2)
                            , Map.insertWith Set.union c2    (Set.singleton c1)
                            ]

populateAdj :: [Edge] -> [Map.Map Cave (Set.Set Cave) -> Map.Map Cave (Set.Set Cave)]
populateAdj = concatMap inserts

toAdj :: [Edge] -> Map.Map Cave (Set.Set Cave)
toAdj edges = foldl1 (.) (populateAdj edges) Map.empty

nexts :: Cave -> Map.Map Cave (Set.Set Cave) -> [Cave]
nexts c m = Set.toList $ fromJust $ Map.lookup c m

pathfind1 :: Map.Map Cave (Set.Set Cave) -> Cave -> [Cave] -> [[Cave]]
pathfind1 _ End p = [p ++ [End]]
pathfind1 m c p
    | isSmall c && c `elem` p = []
    | otherwise  = concatMap (\n -> pathfind1 m n (p ++ [c])) (nexts c m)

pathfind2 :: Map.Map Cave (Set.Set Cave) -> Cave -> [Cave] -> Maybe Cave -> [[Cave]]
pathfind2 _ End p _ = [p ++ [End]]
pathfind2 m c p twice
    | isSmall c && c `elem` p && isJust twice = []
    | otherwise = concatMap (\n -> pathfind2 m n (p ++ [c]) twiceUpdate) (nexts c m)
    where twiceUpdate = if isSmall c && c `elem` p then Just c else twice
