use std::fs::File;
use std::io::{BufRead, BufReader};

fn step(map: &mut Vec<Vec<char>>) -> bool {
    let width = map[0].len();
    let height = map.len();

    let mut new_map_east = map.clone();

    let mut moved = false;

    for y in 0..height {
        for x in 0..width {
            if map[y][x] == '>' && map[y][(x+1)%width] == '.' {
                new_map_east[y][x] = '.';
                new_map_east[y][(x+1)%width] = '>';
                moved = true;
            }
        }
    }

    std::mem::drop(std::mem::replace(map, new_map_east));
    let mut new_map_south = map.clone();

    for y in 0..height {
        for x in 0..width {
            if map[y][x] == 'v' && map[(y+1)%height][x] == '.' {
                new_map_south[y][x] = '.';
                new_map_south[(y+1)%height][x] = 'v';
                moved = true;
            }
        }
    }

    std::mem::drop(std::mem::replace(map, new_map_south));

    moved
}

fn print_map(map: &Vec<Vec<char>>) {
    for row in map {
        for c in row {
            print!("{}", c);
        }
        println!("");
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut map: Vec<Vec<char>> = reader.lines().map(|l| {
        let aline = l.unwrap();
        let trimmed = aline.trim();

        trimmed.chars().collect()
    }).collect();

    let mut n_steps = 0;

    while step(&mut map) {
        n_steps += 1;
    }

    println!("{}", n_steps+1);

    Ok(())
}
