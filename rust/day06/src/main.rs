use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let initial: Vec<i64> = reader.lines().flat_map(|line| line.unwrap().trim().split(',').map(|x| x.parse::<i64>().unwrap()).collect::<Vec<i64>>()).collect();

    let mut fish = [0u64; 512];

    for &f in &initial {
        fish[f as usize] += 1u64;
    }

    for day in 0..=256 {
        fish[day+7] += fish[day];
        fish[day+9] += fish[day];
    }

    fish[0] = initial.len() as u64;
    for i in 1..256 {
        fish[i] += fish[i-1];
    }
    
    println!("{}", fish[255]);

    Ok(())
}
