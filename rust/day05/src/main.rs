use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
struct Point(usize, usize);

#[derive(Debug)]
struct Line(Point, Point);

fn draw(board: &mut [[i64; 1000]; 1000], x1: usize, y1: usize, x2: usize, y2: usize) {
    let dx = if x2 > x1 {
        1i64
    } else if x2 < x1 {
        -1i64
    } else {
        0i64
    };

    let dy = if y2 > y1 {
        1i64
    } else if y2 < y1 {
        -1i64
    } else {
        0i64
    };

    let mut x = x1;
    let mut y = y1;

    while x != x2 || y != y2 {
        board[y][x] += 1;
        x = (x as i64 + dx) as usize;
        y = (y as i64 + dy) as usize;
    }

    board[y2][x2] += 1;
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let lines: Vec<Line> = reader.lines().map(|line| {
        let input_line = line.unwrap().to_owned();
        let point_parts: Vec<&str> = input_line.split("->").collect();
        let p1_parts: Vec<usize> = point_parts[0].trim().split(",").map(|p| p.trim().parse::<usize>().unwrap()).collect();
        let p2_parts: Vec<usize> = point_parts[1].trim().split(",").map(|p| p.trim().parse::<usize>().unwrap()).collect();

        let p1 = Point(p1_parts[0], p1_parts[1]);
        let p2 = Point(p2_parts[0], p2_parts[1]);

        Line(p1, p2)
    }).collect();

    let mut board = [[0i64; 1000]; 1000];

    for line in lines {
        let Line(Point(x1, y1), Point(x2, y2)) = line;

        draw(&mut board, x1, y1, x2, y2);
    }

    let mut count = 0;
    for y in 0..1000 {
        for x in 0..1000 {
            if board[y][x] > 1 {
                count += 1;
            }
        }
    }

    println!("{}", count);

    Ok(())
}
