use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct ALU {
    w: i64,
    x: i64,
    y: i64,
    z: i64,
}

#[derive(Clone, Copy, Debug)]
enum Reg {
    W,
    X,
    Y,
    Z,
}

#[derive(Clone, Copy, Debug)]
enum Arg {
    R(Reg),
    V(i64),
}

#[derive(Clone, Copy, Debug)]
enum Op {
    Inp(Arg),
    Add(Arg, Arg),
    Mul(Arg, Arg),
    Div(Arg, Arg),
    Mod(Arg, Arg),
    Eql(Arg, Arg),
}

fn parse_arg(arg: &str) -> Arg {
    match arg.chars().next().unwrap() {
        'w' | 'x' | 'y' | 'z' => Arg::R(parse_reg(arg)),
        _ => Arg::V(arg.parse::<i64>().unwrap()),
    }
}

fn parse_reg(arg: &str) -> Reg {
    match arg.chars().next().unwrap() {
        'w' => Reg::W,
        'x' => Reg::X,
        'y' => Reg::Y,
        'z' => Reg::Z,
        _ => panic!("Unexpected reg {}", arg),
    }
}

fn parse_op(line: &str) -> Op {
    let parts: Vec<&str> = line.split(' ').collect();

    let arg1 = parse_arg(parts[1]);
    let arg2 = if parts.len() == 3 {
        Some(parse_arg(parts[2]))
    } else {
        None
    };

    match parts[0] {
        "inp" => Op::Inp(arg1),
        "add" => Op::Add(arg1, arg2.unwrap()),
        "mul" => Op::Mul(arg1, arg2.unwrap()),
        "div" => Op::Div(arg1, arg2.unwrap()),
        "mod" => Op::Mod(arg1, arg2.unwrap()),
        "eql" => Op::Eql(arg1, arg2.unwrap()),
        _ => panic!("Unexpected {}", parts[0]),
    }
}

impl ALU {
    fn new() -> ALU {
        ALU {
            w: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    }

    fn exec(self: &mut Self, ops: &Vec<Op>, input: &mut dyn Iterator<Item = &i64>) {
        for op in ops {
            match op {
                Op::Inp(a1) => self.store(a1, *input.next().unwrap()),
                Op::Add(a1, a2) => self.store(a1, self.resolve(a1) + self.resolve(a2)),
                Op::Mul(a1, a2) => self.store(a1, self.resolve(a1) * self.resolve(a2)),
                Op::Div(a1, a2) => self.store(a1, self.resolve(a1) / self.resolve(a2)),
                Op::Mod(a1, a2) => {
                    let n1 = self.resolve(a1);
                    let n2 = self.resolve(a2);
                    if n1 < 0 { panic!("Mod a < 0"); }
                    if n2 <= 0 { panic!("Mod b <= 0"); }
                    self.store(a1, n1 % n2)
                },
                Op::Eql(a1, a2) => self.store(a1, if self.resolve(a1) == self.resolve(a2) { 1 } else { 0 }),
            }
        }
    }

    fn store(self: &mut Self, r: &Arg, val: i64) {
        match r {
            Arg::R(Reg::W) => self.w = val,
            Arg::R(Reg::X) => self.x = val,
            Arg::R(Reg::Y) => self.y = val,
            Arg::R(Reg::Z) => self.z = val,
            _ => panic!("Unexpected dest {:?}", r),
        }
    }

    fn resolve(self: &Self, arg: &Arg) -> i64 {
        match arg {
            Arg::R(Reg::W) => self.w,
            Arg::R(Reg::X) => self.x,
            Arg::R(Reg::Y) => self.y,
            Arg::R(Reg::Z) => self.z,
            Arg::V(val)    => *val,
        }
    }
}

fn solve_r(op_splits: &Vec<Vec<Op>>, serials: &mut Vec<i64>, pos: usize, digits: &mut Vec<i64>, z: i64) {
    if pos == 14 {
        serials.push(digits.iter().fold(0, |acc, d| 10*acc + d));
        return;
    }

    let divz = match op_splits[pos][3] {
        Op::Div(Arg::R(Reg::Z), Arg::V(arg2)) => arg2,
        _ => panic!("Wrong op"),
    };

    let addx = match op_splits[pos][4] {
        Op::Add(Arg::R(Reg::X), Arg::V(arg2)) => arg2,
        _ => panic!("Wrong op"),
    };

    let addy = match op_splits[pos][14] {
        Op::Add(Arg::R(Reg::Y), Arg::V(arg2)) => arg2,
        _ => panic!("Wrong op"),
    };

    if addx < 0 {
        let digit = (z % 26) + addx;
        if digit < 1 || digit > 9 {
            return;
        }

        let new_z = z / divz;

        digits.push(digit);
        solve_r(op_splits, serials, pos + 1, digits, new_z);
        digits.pop();
    } else {
        for digit in 1..=9 {
            let new_z = z / divz * 26 + digit + addy;
            digits.push(digit);
            solve_r(op_splits, serials, pos + 1, digits, new_z);
            digits.pop();
        }
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let ops: Vec<Op> = reader.lines().map(|l| parse_op(&l.unwrap())).collect();

    let op_splits: Vec<Vec<Op>> = ops.split(|op| match op {
        Op::Inp(_) => true,
        _ => false,
    }).map(|p| p.to_vec()).skip(1).collect();

    let mut serials: Vec<i64> = Vec::new();
    solve_r(&op_splits, &mut serials, 0, &mut vec![], 0);

    println!("{}", serials.last().unwrap());
    println!("{}", serials[0]);

    Ok(())
}
