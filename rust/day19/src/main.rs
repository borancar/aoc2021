use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{Add, Neg, Sub};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct Point(i64, i64, i64);

impl Neg for Point {
    type Output = Point;

    fn neg(self) -> Self::Output {
        let Point(x, y, z) = self;

        Point(-x, -y, -z)
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, rhs: Point) -> Self::Output {
        let Point(x1, y1, z1) = self;
        let Point(x2, y2, z2) = rhs;

        Point(x1 - x2, y1 - y2, z1 - z2)
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, rhs: Point) -> Self::Output {
        let Point(x1, y1, z1) = self;
        let Point(x2, y2, z2) = rhs;

        Point(x1 + x2, y1 + y2, z1 + z2)
    }
}

fn read_dets<I>(it: &mut I) -> Vec<Point> where I: Iterator<Item = std::io::Result<String>> {
    let mut points: Vec<Point> = Vec::new();

    it.next();
    while let Some(Ok(l)) = it.next() {
        let trimmed = l.trim();
        if trimmed == "" {
            break;
        }

        let coord: Vec<i64> = trimmed.split(",").map(|p| p.parse::<i64>().unwrap()).collect();
        points.push(Point(coord[0], coord[1], coord[2]));
    }

    points
}

fn orientation(p: &Point, o: i64) -> Point {
    let &Point(x, y, z) = p;

    match o {
         0 => Point( x,  y,  z),
         1 => Point( x,  y, -z),
         2 => Point( x, -y,  z),
         3 => Point( x, -y, -z),
         4 => Point(-x,  y,  z),
         5 => Point(-x,  y, -z),
         6 => Point(-x, -y,  z),
         7 => Point(-x, -y, -z),

         8 => Point( x,  z,  y),
         9 => Point( x,  z, -y),
        10 => Point( x, -z,  y),
        11 => Point( x, -z, -y),
        12 => Point(-x,  z,  y),
        13 => Point(-x,  z, -y),
        14 => Point(-x, -z,  y),
        15 => Point(-x, -z, -y),

        16 => Point( y,  x,  z),
        17 => Point( y,  x, -z),
        18 => Point( y, -x,  z),
        19 => Point( y, -x, -z),
        20 => Point(-y,  x,  z),
        21 => Point(-y,  x, -z),
        22 => Point(-y, -x,  z),
        23 => Point(-y, -x, -z),

        24 => Point( y,  z,  x),
        25 => Point( y,  z, -x),
        26 => Point( y, -z,  x),
        27 => Point( y, -z, -x),
        28 => Point(-y,  z,  x),
        29 => Point(-y,  z, -x),
        30 => Point(-y, -z,  x),
        31 => Point(-y, -z, -x),

        32 => Point( z,  x,  y),
        33 => Point( z,  x, -y),
        34 => Point( z, -x,  y),
        35 => Point( z, -x, -y),
        36 => Point(-z,  x,  y),
        37 => Point(-z,  x, -y),
        38 => Point(-z, -x,  y),
        39 => Point(-z, -x, -y),

        40 => Point( z,  y,  x),
        41 => Point( z,  y, -x),
        42 => Point( z, -y,  x),
        43 => Point( z, -y, -x),
        44 => Point(-z,  y,  x),
        45 => Point(-z,  y, -x),
        46 => Point(-z, -y,  x),
        47 => Point(-z, -y, -x),

        _  => panic!("Invalid orientation {}", o),
    }
}

fn overlap(sc1: &Vec<Point>, sc2: &Vec<Point>, oor: i64) -> Option<(Point, i64)> {
    for or in 0..48 {
        let mut scanner: HashMap<Point, i64> = HashMap::new();

        for &p1 in sc1 {
            for &p2 in sc2 {
                let vec1 = orientation(&p1, oor) - orientation(&p2, or);
                scanner.insert(vec1, scanner.get(&vec1).unwrap_or(&0) + 1);
            }
        }

        if let Some(p) = scanner.iter().find_map(|(k, &v)| {
            if v >= 12 { Some(*k) } else { None }
        }) {
            return Some((p, or));
        }
    }

    None
}

fn resolve_sc(dets: &Vec<Vec<Point>>, pos: &mut Vec<(Point, i64)>, s: usize) {
    let dets1 = &dets[s];
    let (ps1, or1) = pos[s];

    println!("Resolving scanners from {}", s);

    for (s2, dets2) in dets.iter().enumerate() {
        if s == s2 {
            continue;
        }

        if let Some((p2, or2)) = overlap(dets1, dets2, or1) {
            if s2 != 0 && pos[s2].0 == Point(0,0,0) {
                pos[s2] = (ps1 + p2, or2);
                println!("scanner {} - {:?}", s2, pos[s2]);
                resolve_sc(dets, pos, s2);
            }
        }
    }
}

fn beacons(dets: &Vec<Vec<Point>>, pos: &Vec<(Point, i64)>) -> HashSet<Point> {
    let mut points: HashSet<Point> = HashSet::new();

    for (sc, dets) in dets.iter().enumerate() {
        for dp in dets {
            points.insert(orientation(dp, pos[sc].1) + pos[sc].0);
        }
    }

    points
}

fn max_distance(pos: &Vec<(Point, i64)>) -> i64 {
    let mut max_d = 0;

    for (i, sc1) in pos.iter().enumerate() {
        for sc2 in pos[i+1..].iter() {
            let &(Point(x1, y1, z1), _) = sc1;
            let &(Point(x2, y2, z2), _) = sc2;

            max_d = std::cmp::max(max_d, (x2-x1).abs() + (y2-y1).abs() + (z2-z1).abs());
        }
    }

    max_d
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut lines = reader.lines();

    let mut dets: Vec<Vec<Point>> = Vec::new();

    loop {
        let det = read_dets(&mut lines);

        if det.len() == 0 {
            break;
        }

        dets.push(det);
    }

    let mut pos: Vec<(Point, i64)> = Vec::new();
    pos.resize(dets.len(), (Point(0, 0, 0), 0));

    resolve_sc(&dets, &mut pos, 0);

    println!("{}", beacons(&dets, &pos).len());
    println!("{}", max_distance(&pos));

    Ok(())
}
