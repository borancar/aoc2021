use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = std::io::BufReader::new(file);

    let numbers: Vec<i64> = reader.lines().map(|n| n.unwrap().parse::<i64>().unwrap()).collect();

    let window_sums: Vec<i64> = numbers.windows(3).map(|window| {
        match window {
            [x, y, z] => { x + y + z }

            _ => 0
        }
    }).collect();

    let higher: i64 = window_sums.windows(2).map(|pair| {
        match pair {
            [x, y] => {
                if y > x { 1 } else { 0 }
            }

            _ => 0
        }
    }).sum();

    println!("{}", higher);

    Ok(())
}
