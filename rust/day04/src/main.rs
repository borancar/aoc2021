use std::collections::HashSet;
use std::fs::File;
use std::iter::Peekable;
use std::io::{prelude::*, BufReader};

struct Lexer<T: std::io::Read> {
    bytes: Peekable<std::io::Bytes<T>>,
}

impl<T: std::io::Read> Lexer<T> {
    fn new(reader: T) -> Self {
        Lexer {
            bytes: reader.bytes().peekable(),
        }
    }
}

impl<T: std::io::Read> Iterator for Lexer<T> {
    type Item = i64;

    fn next(&mut self) -> Option<i64> {
        match self.bytes.peek() {
            Some(Ok(b'\r')) | Some(Ok(b'\n')) => {
                self.bytes.next();
                None
            },
            Some(Ok(b' ')) => {
                self.bytes.next();
                self.next()
            },
            Some(Err(e)) => {
                panic!("{}", e)
            },
            Some(Ok(ch)) => {
                match ch {
                    b'0'..=b'9' => {
                        let mut number = 0;
                        while let Some(Ok(b'0'..=b'9')) = self.bytes.peek() {
                            if let Some(Ok(digit)) = self.bytes.peek() {
                                number = number * 10 + (digit - b'0') as i64;
                            }
                            self.bytes.next();
                        }
                        Some(number)
                    },
                    _ => {
                        panic!("Invalid character {}", ch);
                    },
                }
            },
            None => None,
        }
    }
}

fn get_board_matrix(reader: &mut impl BufRead) -> [[i64; 5]; 5] {
    let mut board = [[0i64; 5]; 5];

    for i in 0..5 {
        for (j, n) in Lexer::new(&mut *reader).enumerate() {
            board[i][j] = n;
        }
    }

    board
}

#[derive(Debug)]
struct Board {
    won: bool,
    rows: Vec<HashSet<i64>>,
    cols: Vec<HashSet<i64>>,
}

impl Board {
    fn new(matrix: [[i64; 5]; 5]) -> Self {
        let mut board = Board {
            won: false,
            rows: (0..5).map(|_| HashSet::new()).collect(),
            cols: (0..5).map(|_| HashSet::new()).collect(),
        };

        for i in 0..5 {
            for j in 0..5 {
                board.rows[i].insert(matrix[i][j]);
                board.cols[j].insert(matrix[i][j]);
            }
        }

        board
    }
}

fn score(board: &Board) -> i64 {
    board.rows.iter().fold(0, |a, x| {
        a + x.iter().sum::<i64>()
    })
}

fn play(n: i64, boards: &mut Vec<Board>) -> Option<&Board> {
    for b in boards.iter_mut() {
        for r in &mut b.rows {
            r.remove(&n);
            if r.is_empty() {
                b.won = true;
            }
        }

        for c in &mut b.cols {
            c.remove(&n);
            if c.is_empty() {
                b.won = true;
            }
        }
    }

    for b in boards.iter() {
        if b.won {
            return Some(&*b);
        }
    }

    None
}

fn part1(numbers: &Vec<i64>, boards: &mut Vec<Board>) -> i64 {
    for &n in numbers {
        if let Some(winner) = play(n, boards) {
            return n * score(winner);
        }
    }

    0
}

fn part2(numbers: &Vec<i64>, boards: &mut Vec<Board>) -> i64 {
    let mut last_score = 0;

    for &n in numbers {
        if let Some(winner) = play(n, boards) {
            last_score = n * score(winner);
        }

        boards.retain(|b| !b.won);

        if boards.len() == 0 {
            break;
        }
    }

    last_score
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);

    let mut line = String::new();

    reader.read_line(&mut line)?;

    let numbers: Vec<i64> = line.split(",").map(|part| part.trim().parse::<i64>().unwrap()).collect();

    let mut boards: Vec<Board> = Vec::new();
    while let Ok(len) = reader.read_line(&mut line) {
        if len == 0 {
            break;
        }

        boards.push(Board::new(get_board_matrix(&mut reader)));
    }

    //println!("{}", part1(&numbers, &mut boards));
    println!("{}", part2(&numbers, &mut boards));

    Ok(())
}
