use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn move_pos(mut pos: i64, adv: i64) -> i64 {
    pos -= 1;
    pos += adv;
    pos = pos % 10;
    pos += 1;

    pos
}

fn part1(pos: &mut Vec<i64>) -> Option<(i64, i64, i64)> {
    let mut die = (1..=100).cycle();
    let mut score = [0i64; 2];
    let mut throws = 0;

    loop {
        for player in 0..2 {
            let throw: i64 = (&mut die).take(3).sum();
            throws += 3;

            pos[player] = move_pos(pos[player], throw);
            score[player] += pos[player];

            if score[player] >= 1000 {
                let winner = score[player];
                let loser = score[(player + 1) % 2];
                return Some((winner, loser, throws));
            }
        }
    }
}

fn part2(memo: &mut HashMap<(i64, i64, i64, i64), (i64, i64)>, p1: i64, p2: i64, s1: i64, s2: i64) -> (i64, i64) {
    if memo.contains_key(&(p1, p2, s1, s2)) {
        return memo[&(p1, p2, s1, s2)];
    }

    let mut wins = (0, 0);

    for d1 in 1..=3 {
        for d2 in 1..=3 {
            for d3 in 1..=3 {
                let (w1, w2) = wins;

                let np1 = move_pos(p1, d1+d2+d3);
                let ns1 = s1 + np1;
                if ns1 >= 21 {
                    wins = (w1 + 1, w2)
                } else {
                    let (mw2, mw1) = part2(memo, p2, np1, s2, s1 + np1);

                    wins = (w1 + mw1, w2 + mw2);
                }
            }
        }
    }

    memo.insert((p1, p2, s1, s2), wins);

    wins
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let pos: Vec<i64> = reader.lines().map(|l| {
        let aline = l.unwrap();

        let parts: Vec<&str> = aline.split(':').collect();

        parts[1].trim().parse::<i64>().unwrap()
    }).collect();

    let (_, loser, throws) = part1(&mut pos.clone()).unwrap();
    println!("{}", loser * throws);

    let wins = part2(&mut HashMap::new(), pos[0], pos[1], 0, 0);

    println!("{} {}", wins.0, wins.1);

    Ok(())
}
