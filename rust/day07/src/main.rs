use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn move_cost1(src: i64, tgt: i64) -> i64 {
    (tgt - src).abs()
}

fn move_cost2(src: i64, tgt: i64) -> i64 {
    let d = (tgt - src).abs();
    d*(d+1)/2
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let pos: Vec<i64> = reader.lines().flat_map(|l| l.unwrap().trim().split(",").map(|x| x.parse::<i64>().unwrap()).collect::<Vec<i64>>()).collect();

    let max_pos = *pos.iter().max().unwrap();

    let mut crabs_at: Vec<i64> = Vec::new();
    crabs_at.resize(max_pos as usize + 1, 0);

    for p in pos {
        crabs_at[p as usize] += 1;
    }

    let costs: Vec<i64> = (0..=max_pos).map(|t| crabs_at.iter().enumerate().map(|(s, &n)| n * move_cost2(s as i64, t)).sum()).collect();

    println!("{}", costs.iter().min().unwrap());

    Ok(())
}
