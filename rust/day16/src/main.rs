use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::Peekable;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Packet {
    Lit(i64),
    SumOp(Vec<Packet>),
    ProdOp(Vec<Packet>),
    MinOp(Vec<Packet>),
    MaxOp(Vec<Packet>),
    GtOp(Vec<Packet>),
    LtOp(Vec<Packet>),
    EqOp(Vec<Packet>),
}

fn bin_to_dec(iter: &mut dyn Iterator<Item = i64>) -> i64 {
    iter.fold(0, |acc, x| 2*acc + x)
}

fn parse_literal<I>(input: &mut Peekable<I>) -> (i64, usize) where I: Iterator<Item = i64> {
    let mut value = 0;
    let mut len = 0;

    while let Some(1) = input.next() {
        value = 16 * value + bin_to_dec(&mut input.take(4));
        len += 5;
    }

    value = 16 * value + bin_to_dec(&mut input.take(4));
    len += 5;

    (value, len)
}

fn parse_op<I>(input: &mut Peekable<I>) -> (Vec<Packet>, usize) where I: Iterator<Item = i64> {
    let len_type = input.take(1).next().unwrap();
    let mut len = 0;

    match len_type {
        0 => {
            let len_bits = bin_to_dec(&mut input.take(15)) as usize;

            let mut subpackets = Vec::new();
            while len < len_bits {
                let (subpacket, sublen) = parse_packet(input);
                subpackets.push(subpacket);
                len += sublen;
            }

            if len != len_bits {
                panic!("Mismatch len {}, len_bits {}", len, len_bits);
            }

            (subpackets, 1 + 15 + len)
        },
        1 => {
            let n_packets = bin_to_dec(&mut input.take(11));

            let mut subpackets = Vec::new();
            for _i in 0..n_packets {
                let (subpacket, sublen) = parse_packet(input);
                subpackets.push(subpacket);
                len += sublen;
            }
            (subpackets, 1 + 11 + len)
        },
        t => panic!("Invalid len type {}", t),
    }
}

fn parse_packet<I>(input: &mut Peekable<I>) -> (Packet, usize) where I: Iterator<Item = i64> {
    let _version = bin_to_dec(&mut input.take(3));
    let typeid = bin_to_dec(&mut input.take(3));

    //println!("{}", version);

    match typeid {
        0 => {
            let (sub, len) = parse_op(input);
            (Packet::SumOp(sub), len + 6)
        },
        1 => {
            let (sub, len) = parse_op(input);
            (Packet::ProdOp(sub), len + 6)
        },
        2 => {
            let (sub, len) = parse_op(input);
            (Packet::MinOp(sub), len + 6)
        },
        3 => {
            let (sub, len) = parse_op(input);
            (Packet::MaxOp(sub), len + 6)
        },
        4 => {
            let (literal, len) = parse_literal(input);
            (Packet::Lit(literal), len + 6)
        },
        5 => {
            let (sub, len) = parse_op(input);
            (Packet::GtOp(sub), len + 6)
        },
        6 => {
            let (sub, len) = parse_op(input);
            (Packet::LtOp(sub), len + 6)
        },
        7 => {
            let (sub, len) = parse_op(input);
            (Packet::EqOp(sub), len + 6)
        },
        t => {
            panic!("Uknown type {}", t);
        }
    }
}

fn exec_packet(packet: &Packet) -> i64 {
    match packet {
        Packet::SumOp(s) => s.iter().map(|p| exec_packet(p)).sum(),
        Packet::ProdOp(s) => s.iter().map(|p| exec_packet(p)).product(),
        Packet::MinOp(s) => s.iter().map(|p| exec_packet(p)).min().unwrap(),
        Packet::MaxOp(s) => s.iter().map(|p| exec_packet(p)).max().unwrap(),
        Packet::Lit(n) => *n,
        Packet::GtOp(s) => if exec_packet(&s[0]) > exec_packet(&s[1]) { 1 } else { 0 },
        Packet::LtOp(s) => if exec_packet(&s[0]) < exec_packet(&s[1]) { 1 } else { 0 },
        Packet::EqOp(s) => if exec_packet(&s[0]) == exec_packet(&s[1]) { 1 } else { 0 },
    }
}

fn parse_string(input: &str) -> Vec<i64> {
    input.chars().map(|c| {
        match c {
            '0' => vec![0, 0, 0, 0],
            '1' => vec![0, 0, 0, 1],
            '2' => vec![0, 0, 1, 0],
            '3' => vec![0, 0, 1, 1],
            '4' => vec![0, 1, 0, 0],
            '5' => vec![0, 1, 0, 1],
            '6' => vec![0, 1, 1, 0],
            '7' => vec![0, 1, 1, 1],
            '8' => vec![1, 0, 0, 0],
            '9' => vec![1, 0, 0, 1],
            'A' => vec![1, 0, 1, 0],
            'B' => vec![1, 0, 1, 1],
            'C' => vec![1, 1, 0, 0],
            'D' => vec![1, 1, 0, 1],
            'E' => vec![1, 1, 1, 0],
            'F' => vec![1, 1, 1, 1],
            c   => panic!("Unknown input char {}", c),
        }
    }).flatten().collect()
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let input: Vec<i64> = reader.lines().map(|l| {
        let aline = l.unwrap();
        let trimmed = aline.trim();

        parse_string(trimmed)
    }).flatten().collect();

    let packet = parse_packet(&mut input.iter().copied().peekable());

    println!("{}", exec_packet(&packet.0));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_literal() {
        let input = parse_string("D2FE28");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::Lit(2021), packet.0);
    }

    #[test]
    fn test_oplen_2lits() {
        let input = parse_string("38006F45291200");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::LtOp(vec![Packet::Lit(10), Packet::Lit(20)]), packet.0);
    }

    #[test]
    fn test_opn_3lits() {
        let input = parse_string("EE00D40C823060");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::MaxOp(vec![Packet::Lit(1), Packet::Lit(2), Packet::Lit(3)]), packet.0);
    }

    #[test]
    fn test_single_nested() {
        let input = parse_string("8A004A801A8002F478");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::MinOp(vec![Packet::MinOp(vec![Packet::MinOp(vec![Packet::Lit(15)])])]), packet.0);
    }

    #[test]
    fn test_double_nested() {
        let input = parse_string("620080001611562C8802118E34");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::SumOp(vec![Packet::SumOp(vec![Packet::Lit(10), Packet::Lit(11)]), Packet::SumOp(vec![Packet::Lit(12), Packet::Lit(13)])]), packet.0);
    }

    #[test]
    fn test_double_nested_2() {
        let input = parse_string("C0015000016115A2E0802F182340");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::SumOp(vec![Packet::SumOp(vec![Packet::Lit(10), Packet::Lit(11)]), Packet::SumOp(vec![Packet::Lit(12), Packet::Lit(13)])]), packet.0);
    }

    #[test]
    fn test_op_op_op_5lits() {
        let input = parse_string("A0016C880162017C3686B18A3D4780");
        let packet = parse_packet(&mut input.iter().copied().peekable());
        assert_eq!(Packet::SumOp(vec![Packet::SumOp(vec![Packet::SumOp(vec![Packet::Lit(6), Packet::Lit(6), Packet::Lit(12), Packet::Lit(15), Packet::Lit(15)])])]), packet.0);
    }
}
