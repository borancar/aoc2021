use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
enum Cave {
    Start,
    Small(String),
    Big(String),
    End,
}

impl Cave {
    fn name(&self) -> &str {
        match self {
            Cave::Start => "start",
            Cave::End => "end",
            Cave::Small(n) => &n,
            Cave::Big(n) => &n,
        }
    }

    fn is_start(&self) -> bool {
        match self {
            Cave::Start => true,
            _ => false,
        }
    }

    fn is_end(&self) -> bool {
        match self {
            Cave::End => true,
            _ => false,
        }
    }

    fn is_small(&self) -> bool {
        match self {
            Cave::Small(_) => true,
            _ => false,
        }
    }
}

fn pathfind_r(next: &HashMap<Rc<Cave>, HashSet<Rc<Cave>>>,
              paths: &mut Vec<Vec<Rc<Cave>>>,
              node: Rc<Cave>,
              path: &mut Vec<Rc<Cave>>,
              twice: Option<Rc<Cave>>) {

    if node.is_end() {
        path.push(node.clone());
        paths.push(path.clone());
        path.pop();
        return;
    }

    path.push(node.clone());

    for n in &next[&node] {
        let mut new_twice: Option<Rc<Cave>> = twice.clone();

        if n.is_small() {
            if path.contains(n) {
                if twice.is_some() {
                    continue;
                } else {
                    new_twice = Some(n.clone());
                }
            }
        }

        pathfind_r(next, paths, n.clone(), path, new_twice);
    }

    path.pop();
}

fn parse_cave(name: &str) -> Cave {
    match name {
        "start" => Cave::Start,
        "end" => Cave::End,
        n if n.chars().all(|c| c.is_lowercase()) => Cave::Small(n.to_string()),
        n if n.chars().all(|c| c.is_uppercase()) => Cave::Big(n.to_string()),
        n => panic!("Invalid cave name {}", n),
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut next: HashMap<Rc<Cave>, HashSet<Rc<Cave>>> = HashMap::new();

    for aline in reader.lines() {
        let line = aline.unwrap();
        let parts: Vec<&str> = line.trim().split("-").collect();

        let cave1 = Rc::new(parse_cave(parts[0]));
        let cave2 = Rc::new(parse_cave(parts[1]));

        if !next.contains_key(&cave1) {
            next.insert(cave1.clone(), HashSet::new());
        }

        if !next.contains_key(&cave2) {
            next.insert(cave2.clone(), HashSet::new());
        }

        if !cave1.is_end() && !cave2.is_start() {
            next.get_mut(&cave1).unwrap().insert(cave2.clone());
        }

        if !cave2.is_end() && !cave1.is_start() {
            next.get_mut(&cave2).unwrap().insert(cave1.clone());
        }
    }

    let mut paths: Vec<Vec<Rc<Cave>>> = Vec::new();

    pathfind_r(&next, &mut paths, Rc::new(Cave::Start), &mut Vec::new(), None);

    println!("{}", paths.len());

    Ok(())
}
