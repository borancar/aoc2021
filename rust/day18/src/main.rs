#![feature(box_patterns)]

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::Peekable;

#[derive(Clone, Debug, Eq, PartialEq)]
enum Snum {
    Regular(i64),
    Pair(Box<Snum>, Box<Snum>),
}

fn snum_to_string(node: &Snum) -> String {
    let mut output = String::new();

    match node {
        Snum::Pair(left, right) => {
            output.push('[');
            output.push_str(&snum_to_string(left));
            output.push(',');
            output.push_str(&snum_to_string(right));
            output.push(']');
        },
        Snum::Regular(v) => {
            output.push_str(&format!("{}", v));
        },
    }

    output
}

fn parse_snum<I>(it: &mut Peekable<I>) -> Snum where I: Iterator<Item = char> {
    match it.peek() {
        Some('[') => {
            it.next();
            let left = parse_snum(it);
            it.next();
            let right = parse_snum(it);
            it.next();
            Snum::Pair(Box::new(left), Box::new(right))
        },
        Some('0'..='9') => {
            let mut num = 0;
            while let Some(ch @ '0'..='9') = it.peek() {
                num = 10*num + (*ch as i64 - '0' as i64);

                it.next();
            }
            Snum::Regular(num)
        },
        Some(ch) => panic!("Unknown character {}", ch),
        None => panic!("Terminated early!"),
    }
}

fn add_reduce(a: Snum, b: Snum) -> Snum {
    let mut sum = Snum::Pair(Box::new(a), Box::new(b));

    loop {
        let explodes = match explode(&mut sum, 0) {
            Some(_) => true,
            _ => false,
        };

        if explodes {
            continue;
        }

        let splits = split(&mut sum);

        if !explodes && !splits {
            break;
        }
    }

    sum
}

fn update_leftmost(node: &mut Snum, add: i64) {
    match node {
        Snum::Pair(left, _) => {
            update_leftmost(left, add);
        },
        Snum::Regular(v) => {
            *v += add;
        },
    }
}

fn update_rightmost(node: &mut Snum, add: i64) {
    match node {
        Snum::Pair(_, right) => {
            update_rightmost(right, add);
        },
        Snum::Regular(v) => {
            *v += add;
        },
    }
}

fn explode(node: &mut Snum, depth: usize) -> Option<(i64, i64)> {
    match node {
        Snum::Pair(box Snum::Regular(left), box Snum::Regular(right)) if depth >= 4 => {
            let regular = Snum::Regular(0);
            let ret = (*left, *right);
            std::mem::drop(std::mem::replace(node, regular));
            return Some(ret);
        },
        Snum::Pair(left, right) => {
            if let Some((addl, addr)) = explode(left, depth + 1) {
                update_leftmost(right, addr);
                return Some((addl, 0));
            } else if let Some((addl, addr)) = explode(right, depth + 1) {
                update_rightmost(left, addl);
                return Some((0, addr));
            }
        },
        _ => {},
    }

    return None;
}

fn split(node: &mut Snum) -> bool {
    match node {
        Snum::Pair(left, right) => {
            if split(left) { return true; }
            if split(right) { return true; }
        },
        &mut Snum::Regular(num) if num >= 10 => {
            let split_pair = Snum::Pair(Box::new(Snum::Regular(num / 2)), Box::new(Snum::Regular(num - num / 2)));
            std::mem::drop(std::mem::replace(node, split_pair));
            return true;
        },
        _ => {},
    }

    return false;
}

fn mag(node: &Snum) -> i64 {
    match node {
        Snum::Pair(left, right) => 3 * mag(left) + 2 * mag(right),
        Snum::Regular(v) => *v,
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let snums: Vec<Snum> = reader.lines().map(|l| {
        parse_snum(&mut l.unwrap().chars().peekable())
    }).collect();

    ////part1
    //let result = snums.into_iter().reduce(|a, b| add_reduce(a,b)).unwrap();
    //println!("{}", mag(&result));

    //part2
    let mut max_mag = 0;

    for i in 0..snums.len() {
        for j in (i+1)..snums.len() {
            let mag1 = mag(&add_reduce(snums[i].clone(), snums[j].clone()));
            let mag2 = mag(&add_reduce(snums[j].clone(), snums[i].clone()));
            max_mag = std::cmp::max(max_mag, mag1);
            max_mag = std::cmp::max(max_mag, mag2);
        }
    }

    println!("{}", max_mag);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_explode(input: &str, output: &str) {
        let mut snum = parse_snum(&mut input.chars().peekable());
        explode(&mut snum, 0);
        assert_eq!(output, snum_to_string(&snum));
    }

    #[test]
    fn test_explodes() {
        test_explode("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
        test_explode("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
        test_explode("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
        test_explode("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
        test_explode("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
    }
}
