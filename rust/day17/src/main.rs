use std::fs::File;
use std::io::{BufRead, BufReader};

use regex::Regex;

fn fire(mut vx: i64, mut vy: i64, sx: i64, sy: i64, ex: i64, ey: i64) -> (bool, i64) {
    let mut cx = 0;
    let mut cy = 0;
    let mut max_y = 0;

    while cx <= ex && cy >= sy {
        if cx >= sx && cx <= ex &&
            cy >= sy && cy <= ey {
                return (true, max_y);
            }

        cx += vx;
        cy += vy;
        max_y = std::cmp::max(max_y, cy);

        vx = if vx > 0 { vx - 1 } else if vx < 0 { vx + 1 } else { 0 };
        vy = vy - 1;
    }

    (false, 0)
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line)?;

    let re = Regex::new(r"target area: x=(\d*)\.\.(\d*), y=(-?\d*)\.\.(-?\d*)").unwrap();

    let cap = re.captures_iter(&line).next().unwrap();
    let sx = cap[1].parse::<i64>().unwrap();
    let ex = cap[2].parse::<i64>().unwrap();
    let sy = cap[3].parse::<i64>().unwrap();
    let ey = cap[4].parse::<i64>().unwrap();

    let min_vx = ((((1 + 8*sx) as f64).sqrt() - 1.0)/2.0 + 0.5) as i64;
    let max_vy = -sy-1;

    // part1
    println!("{}", fire(min_vx+1, max_vy, sx, sy, ex, ey).1);

    let max_vx = ex;
    let min_vy = sy;

    let mut combs = 0;

    for vx in min_vx..=max_vx {
        for vy in min_vy..=max_vy {
            if fire(vx, vy, sx, sy, ex, ey).0 {
                combs += 1;
            }
        }
    }

    // part2
    println!("{}", combs);

    Ok(())
}
