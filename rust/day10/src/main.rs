use std::collections::VecDeque;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn closing_pair(ch: char) -> char {
    match ch {
        '(' => ')',
        '[' => ']',
        '{' => '}',
        '<' => '>',
        c   => panic!("Unknown {}", c),
    }
}

fn complete(line: &str) -> Result<VecDeque<char>, char> {
    let mut stack: VecDeque<char> = VecDeque::new();

    for c in line.chars() {
        match c {
            ch @ ('(' | '[' | '{' | '<') => {
                stack.push_front(closing_pair(ch));
            },

            ch @ (')' | ']' | '}' | '>') => {
                if let Some(&top) = stack.front() {
                    if ch != top {
                        return Err(ch);
                    } else {
                        stack.pop_front();
                    }
                }
            },

            ch => panic!("Unexpected char {}", ch),
        }
    }

    Ok(stack)
}

fn corrupted(line: &str) -> i64 {
    match complete(line) {
        Ok(_)    => 0,
        Err(')') => 3,
        Err(']') => 57,
        Err('}') => 1197,
        Err('>') => 25137,
        Err(c)   => panic!("Undefined char {}", c),
    }
}

fn completion_score(line: &str) -> i64 {
    match complete(line) {
        Ok(s)  => s.iter().fold(0, |acc, c| {
            let csc = match c {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                ch  => panic!("Unexpected completion char {}", ch),
            };

            5 * acc + csc
        }),
        Err(_) => 0,
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    //// part1
    //println!("{}", reader.lines().map(|l| check(l.unwrap().trim())).sum::<i64>());

    let mut completion_scores: Vec<i64> = reader.lines().filter_map(|l| {
        let line = l.unwrap().trim().to_string();

        let csc = completion_score(&line);

        if csc > 0 {
            Some(csc)
        } else {
            None
        }
    }).collect();

    completion_scores.sort();

    // part2
    println!("{}", completion_scores[completion_scores.len()/2]);

    Ok(())
}
