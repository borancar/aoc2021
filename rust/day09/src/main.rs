use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::collections::HashSet;

fn low_point(map: &Vec<Vec<i64>>, x: usize, y: usize) -> bool {
    if x > 0 && !(map[y][x] < map[y][x-1]) {
        return false;
    }

    if y > 0 && !(map[y][x] < map[y-1][x]) {
        return false;
    }

    if x < map[y].len() - 1 && !(map[y][x] < map[y][x+1]) {
        return false;
    }

    if y < map.len() - 1 && !(map[y][x] < map[y+1][x]) {
        return false;
    }

    return true;
}

fn low_points(map: &Vec<Vec<i64>>) -> Vec<(usize, usize)> {
    let mut ps: Vec<(usize, usize)> = Vec::new();

    for (y, row) in map.iter().enumerate() {
        for (x, _v) in row.iter().enumerate() {
            if low_point(map, x, y) {
                ps.push((x, y));
            }
        }
    }

    ps
}

fn fill_basin(map: &Vec<Vec<i64>>, basin: &mut HashSet<(usize, usize)>, x: usize, y: usize) {
    if basin.contains(&(x, y)) {
        return;
    }

    if map[y][x] == 9 {
        return;
    }

    basin.insert((x, y));

    if x > 0 && map[y][x-1] > map[y][x] {
        fill_basin(map, basin, x-1, y);
    }
    if y > 0 && map[y-1][x] > map[y][x] {
        fill_basin(map, basin, x, y-1);
    }
    if x < map[y].len()-1 && map[y][x+1] > map[y][x] {
        fill_basin(map, basin, x+1, y);
    }
    if y < map.len()-1 && map[y+1][x] > map[y][x] {
        fill_basin(map, basin, x, y+1);
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let map: Vec<Vec<i64>> = reader.lines().map(|l| l.unwrap().trim().chars().map(|x| x as i64 - '0' as i64).collect()).collect();

    let mut basins: Vec<HashSet<(usize, usize)>> = Vec::new();

    for (x, y) in low_points(&map) {
        let mut new_basin: HashSet<(usize, usize)> = HashSet::new();
        fill_basin(&map, &mut new_basin, x, y);
        basins.push(new_basin);
    }

    let mut basin_sizes: Vec<usize> = basins.iter().map(|b| b.len()).collect();
    basin_sizes.sort_by(|a, b| b.cmp(a));

    println!("{:?}", basin_sizes.iter().copied().take(3).reduce(|a, x| a*x).unwrap());

    Ok(())
}
