use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct Range(i64, i64);

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct Cuboid(Range, Range, Range);

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Switch {
    On(Cuboid),
    Off(Cuboid),
}

#[derive(Clone, Debug)]
struct Node {
    range: Range,
    removed: bool,
    less: Option<Box<Node>>,
    within: Option<Box<Node>>,
    greater: Option<Box<Node>>,
}

fn parse_line(line: &str) -> Switch {
    let main_parts: Vec<&str> = line.split(' ').collect();
    let xyz_parts: Vec<&str> = main_parts[1].split(',').collect();
    let assigns: Vec<&str> = xyz_parts.iter().map(|a| a.split('=').collect::<Vec<&str>>()[1]).collect();
    let mut ranges: Vec<Option<Range>> = assigns.iter().map(|r| {
        let parts: Vec<&str> = r.split('.').collect();
        Some(Range(parts[0].parse::<i64>().unwrap(), parts[2].parse::<i64>().unwrap()))
    }).collect();

    let range_x = ranges[0].take().unwrap();
    let range_y = ranges[1].take().unwrap();
    let range_z = ranges[2].take().unwrap();

    match main_parts[0] {
        "on" => Switch::On(Cuboid(range_x, range_y, range_z)),
        "off" => Switch::Off(Cuboid(range_x, range_y, range_z)),
        s => panic!("Invalid {}", s),
    }
}

fn clip(r: &Range) -> Range {
    let &Range(start, end) = r;

    Range(std::cmp::max(start, -50), std::cmp::min(end, 50))
}

fn intersect_range(r1: &Range, r2: &Range) -> Option<Range> {
    let Range(r1s, r1e) = r1;
    let Range(r2s, r2e) = r2;

    if r1s <= r2s && r1e >= r2s {
        if r1e <= r2e {
            Some(Range(*r2s, *r1e))
        } else {
            Some(Range(*r2s, *r2e))
        }
    } else if r1s > r2s && r1s <= r2e {
        if r1e <= r2e {
            Some(Range(*r1s, *r1e))
        } else {
            Some(Range(*r1s, *r2e))
        }
    } else {
        None
    }
}

fn fill(r: &mut [[[i64; 101]; 101]; 101], range_x: &Range, range_y: &Range, range_z: &Range, value: i64) {
    let &Range(sx, ex) = range_x;
    let &Range(sy, ey) = range_y;
    let &Range(sz, ez) = range_z;

    for x in sx+50..=ex+50 {
        for y in sy+50..=ey+50 {
            for z in sz+50..=ez+50 {
                r[x as usize][y as usize][z as usize] = value;
            }
        }
    }
}

fn part1(switches: &Vec<Switch>) -> i64 {
   let mut region = [[[0i64; 101]; 101]; 101];

   for switch in switches {
        match switch {
            Switch::On(Cuboid(range_x, range_y, range_z)) => {
                let clipped_x = clip(range_x);
                let clipped_y = clip(range_y);
                let clipped_z = clip(range_z);

                fill(&mut region, &clipped_x, &clipped_y, &clipped_z, 1);
            },
            Switch::Off(Cuboid(range_x, range_y, range_z)) => {
                let clipped_x = clip(range_x);
                let clipped_y = clip(range_y);
                let clipped_z = clip(range_z);

                fill(&mut region, &clipped_x, &clipped_y, &clipped_z, 0);
            },
        }
    }

    let lit: i64 = region.iter().map(|r| r.iter().map(|r| r.iter().sum::<i64>()).sum::<i64>()).sum();

    lit
}

fn valid_range(r: &Range) -> bool {
    let Range(rs, re) = r;

    rs <= re
}

fn insert(node: &mut Option<Box<Node>>, cube: &mut Vec<Range>, coord: usize, remove: bool) {
    if coord == 3 {
        return;
    }

    let r = cube[coord];

    match node {
        None => {
            if remove {
                return;
            }

            let mut new_node = Box::new(Node{
                range: r,
                removed: false,
                less: None,
                within: None,
                greater: None,
            });

            insert(&mut new_node.within, cube, coord + 1, remove);

            node.replace(new_node);
        },
        Some(n) => {
            let &Range(rs, re) = &r;
            let &Range(nrs, nre) = &n.range;
            
            if let Some(ir) = intersect_range(&r, &n.range) {
                let Range(irs, ire) = ir;

                let mut new_node = Box::new(Node {
                    range: ir,
                    removed: if coord == 2 { remove } else { false },
                    less: None,
                    within: n.within.clone(),
                    greater: None,
                });

                let lt_range = Range(nrs, irs-1);
                let gt_range = Range(ire+1, nre);

                if valid_range(&lt_range) {
                    let new_node_lt = Box::new(Node {
                        range: lt_range,
                        removed: n.removed,
                        less: n.less.take(),
                        within: n.within.clone(),
                        greater: None,
                    });

                    new_node.less = Some(new_node_lt);
                } else {
                    new_node.less = n.less.take();
                }

                if valid_range(&gt_range) {
                    let new_node_gt = Box::new(Node {
                        range: gt_range,
                        removed: n.removed,
                        less: None,
                        within: n.within.clone(),
                        greater: n.greater.take(),
                    });

                    new_node.greater = Some(new_node_gt);
                } else {
                    new_node.greater = n.greater.take();
                }

                let lt_cube_range = Range(rs, irs-1);
                if valid_range(&lt_cube_range) {
                    let orig = cube[coord];
                    cube[coord] = lt_cube_range;
                    insert(&mut new_node.less, cube, coord, remove);
                    cube[coord] = orig;
                }

                let gt_cube_range = Range(ire+1, re);
                if valid_range(&gt_cube_range) {
                    let orig = cube[coord];
                    cube[coord] = gt_cube_range;
                    insert(&mut new_node.greater, cube, coord, remove);
                    cube[coord] = orig;
                }

                insert(&mut new_node.within, cube, coord+1, remove);

                node.replace(new_node);
            } else if re < nrs {
                insert(&mut n.less, cube, coord, remove);
            } else if rs > nre {
                insert(&mut n.greater, cube, coord, remove);
            } else {
                panic!("Impossible");
            }
        },
    }
}

fn print_inorder(node: &Option<Box<Node>>, coord: i64) {
    match node {
        None => {},
        Some(n) => {
            print_inorder(&n.less, coord);
            for _i in 0..coord {
                print!("                    ");
            }
            println!("->{:?}", n.range);
            print_inorder(&n.within, coord+1);
            print_inorder(&n.greater, coord);
        },
    }
}

fn volume(node: &Option<Box<Node>>, coord: usize) -> i64 {
    if coord == 3 {
        return 1;
    }

    match node {
        None => 0,
        Some(n) => {
            let mut vol = 0;
            vol += volume(&n.less, coord);
            vol += if !n.removed { (n.range.1 - n.range.0 + 1) * volume(&n.within, coord+1) } else { 0 };
            vol += volume(&n.greater, coord);

            vol
        }
    }
}

fn part2(switches: &Vec<Switch>) -> i64 {
    let mut tree: Option<Box<Node>> = None;

    for switch in switches.iter() {
        match switch {
            Switch::On(Cuboid(range_x, range_y, range_z)) => {
                insert(&mut tree, &mut vec![*range_x, *range_y, *range_z], 0, false);
            },
            Switch::Off(Cuboid(range_x, range_y, range_z)) => {
                insert(&mut tree, &mut vec![*range_x, *range_y, *range_z], 0, true);
            },
        }
    }

    volume(&tree, 0)
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let switches: Vec<Switch> = reader.lines().map(|l| parse_line(&l.unwrap())).collect();
    println!("{}", part1(&switches));
    println!("{}", part2(&switches));

    Ok(())
}
