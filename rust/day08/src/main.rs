use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::collections::{HashMap, HashSet};

fn number_map() -> HashMap<char, HashSet<char>> {
    let mut map: HashMap<char, HashSet<char>> = HashMap::new();

    map.insert('0', HashSet::from(['a', 'b', 'c', 'e', 'f', 'g']));
    map.insert('1', HashSet::from(['c', 'f']));
    map.insert('2', HashSet::from(['a', 'c', 'd', 'e', 'g']));
    map.insert('3', HashSet::from(['a', 'c', 'd', 'f', 'g']));
    map.insert('4', HashSet::from(['b', 'c', 'd', 'f']));
    map.insert('5', HashSet::from(['a', 'b', 'd', 'f', 'g']));
    map.insert('6', HashSet::from(['a', 'b', 'd', 'e', 'f', 'g']));
    map.insert('7', HashSet::from(['a', 'c', 'f']));
    map.insert('8', HashSet::from(['a', 'b', 'c', 'd', 'e', 'f', 'g']));
    map.insert('9', HashSet::from(['a', 'b', 'c', 'd', 'f', 'g']));

    map
}

fn inv_number_map(nmap: &HashMap<char, HashSet<char>>) -> HashMap<String, char> {
    let mut invmap: HashMap<String, char> = HashMap::new();

    for (k, v) in nmap {
        let mut vs: Vec<char> = v.iter().cloned().collect();
        vs.sort();
        let svs: String = vs.iter().collect();
        invmap.insert(svs, *k);
    }

    invmap
}

fn crossmap_init() -> HashMap<char, HashSet<char>> {
    let mut map: HashMap<char, HashSet<char>> = HashMap::new();

    for c in 'a'..='g' {
        map.insert(c, HashSet::from(['a', 'b', 'c', 'd', 'e', 'f', 'g']));
    }

    map
}

fn claim_segs(cross: &mut HashMap<char, HashSet<char>>, real: &str, expect: &HashSet<char>) {
    for i in real.chars() {
        cross.get_mut(&i).unwrap().retain(|x| expect.contains(x));
    }
}

fn solve(l: &str) -> i64 {
    let parts: Vec<&str> = l.split("|").collect();

    let input: Vec<&str> = parts[0].trim().split(" ").collect();
    let output: Vec<&str> = parts[1].trim().split(" ").collect();

    let nmap = number_map();
    let invnmap = inv_number_map(&nmap);
    let mut cross = crossmap_init();

    for i in input {
        match i.len() {
            // 0, 6, 9
            6 => {
                let mut union: HashSet<char> = nmap[&'0'].union(&nmap[&'6']).map(|x| *x).collect();
                union = union.union(&nmap[&'9']).map(|x| *x).collect();
                claim_segs(&mut cross, i, &union);
            },
            // 1
            2 => {
                claim_segs(&mut cross, i, &nmap[&'1']);
            },
            // 2, 3, 5
            5 => {
                let mut union: HashSet<char> = nmap[&'2'].union(&nmap[&'3']).map(|x| *x).collect();
                union = union.union(&nmap[&'5']).map(|x| *x).collect();
                claim_segs(&mut cross, i, &union);
            },
            // 4
            4 => {
                claim_segs(&mut cross, i, &nmap[&'4']);
            },
            // 7
            3 => {
                claim_segs(&mut cross, i, &nmap[&'7']);
            },
            // 8
            7 => {
                claim_segs(&mut cross, i, &nmap[&'8']);
            },
            _ => {
                panic!("Unknown length!");
            },
        }
    }

    for _i in 0..2 {
        let set2: Vec<HashSet<char>> = cross.iter().filter(|(_, v)| v.len() == 2).map(|(_, v)| v.clone()).collect();

        for seg in 'a'..='g' {
            if cross[&seg].len() > 2 {
                cross.get_mut(&seg).unwrap().retain(|x| !set2.iter().any(|s| s.contains(x)));
            }
        }

        let set1: Vec<HashSet<char>> = cross.iter().filter(|(_, v)| v.len() == 1).map(|(_, v)| v.clone()).collect();

        for seg in 'a'..='g' {
            if cross[&seg].len() > 1 {
                cross.get_mut(&seg).unwrap().retain(|x| !set1.iter().any(|s| s.contains(x)));
            }
        }

    }

    if let Some(n) = try_decode(&invnmap, &mut HashMap::new(), &cross, 'a', &output) {
        n
    } else {
        panic!("Could not decode");
    }
}

fn try_decode(invnmap: &HashMap<String, char>, cmap: &mut HashMap<char, char>, cross: &HashMap<char, HashSet<char>>, c: char, output: &Vec<&str>) -> Option<i64> {
    if c == 'h' {
        let fix_output: Vec<Option<&char>> = output.iter().map(|o| {
            let mut segs: Vec<char> = o.chars().map(|c| {
                cmap[&c]
            }).collect();

            segs.sort();

            let segs_str: String = segs.into_iter().collect();

            invnmap.get(&segs_str)
        }).collect();

        return fix_output.iter().fold(Some(0), |acc, x| {
            if let Some(&n) = x {
                if let Some(acc_v) = acc {
                    Some(acc_v * 10 + (n as i64 - '0' as i64) as i64)
                } else {
                    None
                }
            } else {
                None
            }
        });
    }

    for attempt in &cross[&c] {
        if !cmap.values().any(|v| v == attempt) {
            cmap.insert(c, *attempt);
            if let Some(n) = try_decode(&invnmap, cmap, &cross, (c as u8 + 1) as char, &output) {
                return Some(n)
            }
            cmap.remove(&c);
        }
    }

    None
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().map(|x| x.unwrap().trim().to_string()).collect();

    println!("{}", lines.iter().map(|l| solve(l)).sum::<i64>());

    Ok(())
}
