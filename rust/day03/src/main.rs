use std::fs::File;
use std::io::{prelude::*, BufReader};

fn acc(report: &Vec<Vec<i64>>) -> Vec<i64> {
    let mut acc = report[0].clone();

    for e in report.iter().skip(1) {
        acc = e.iter().zip(acc.iter()).map(|(i1, i2)| i1 + i2).collect();
    }

    acc
}

fn power(report: &Vec<Vec<i64>>) -> i64 {
    let acc = acc(report);

    let gamma = acc.iter().map(|&d| if d > 0 { 1 } else { 0 }).fold(0, |n, d| {
        2 * n + d
    });

    let epsilon = acc.iter().map(|&d| if d > 0 { 0 } else { 1 }).fold(0, |n, d| {
        2 * n + d
    });

    gamma * epsilon
}

fn oxygen(report: &Vec<Vec<i64>>) -> i64 {
    let mut work: Vec<Vec<i64>> = report.clone();

    for i in 0..work[0].len() {
        let cacc = acc(&work);

        work = work.iter().filter_map(|n| {
            if cacc[i] >= 0 {
                if n[i] == 1 {
                    Some(n.clone())
                } else {
                    None
                }
            } else {
                if n[i] == -1 {
                    Some(n.clone())
                } else {
                    None
                }
            }
        }).collect();
    }

    work[0].iter().map(|&d| if d > 0 { 1 } else { 0 }).fold(0, |n, d| {
        2 * n + d
    })
}

fn co2_scrubber(report: &Vec<Vec<i64>>) -> i64 {
    let mut work: Vec<Vec<i64>> = report.clone();

    for i in 0..work[0].len() {
        let cacc = acc(&work);

        if work.len() == 1 {
            break
        }

        work = work.iter().filter_map(|n| {
            if cacc[i] >= 0 {
                if n[i] == -1 {
                    Some(n.clone())
                } else {
                    None
                }
            } else {
                if n[i] == 1 {
                    Some(n.clone())
                } else {
                    None
                }
            }
        }).collect();
    }

    work[0].iter().map(|&d| if d > 0 { 1 } else { 0 }).fold(0, |n, d| {
        2 * n + d
    })
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let report: Vec<Vec<i64>> = reader.lines().map(|line| {
        let bits: Vec<i64> = line.unwrap().trim().chars().map(|c| if c == '1' { 1 } else { -1 }).collect();

        bits
    }).collect();

    println!("{}", power(&report));

    println!("{} {} {}", oxygen(&report), co2_scrubber(&report), oxygen(&report) * co2_scrubber(&report));

    Ok(())
}
