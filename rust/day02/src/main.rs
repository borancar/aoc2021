use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
enum Command {
    Forward(i64),
    Up(i64),
    Down(i64),
}

#[derive(Debug)]
struct Sub {
    aim: i64,
    horiz: i64,
    vert: i64,

}

impl Sub {
    fn command(&mut self, cmd: &Command) {
        match cmd {
            Command::Forward(d) => { self.horiz += d; self.vert += d * self.aim },
            Command::Up(a) => { self.aim -= a },
            Command::Down(a) => { self.aim += a },
        }
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = std::io::BufReader::new(file);

    let commands: Vec<Command> = reader.lines().map(|line| {
        let cmd = line.unwrap();
        let parts: Vec<&str> = cmd.split(" ").collect();
        let units = parts[1].parse::<i64>().unwrap();
        match parts[0] {
            "forward" => Command::Forward(units),
            "up" => Command::Up(units),
            "down" => Command::Down(units),
            _ => panic!("Unknown {}", parts[0]),
        }
    }).collect();

    let mut sub = Sub { horiz: 0, vert: 0, aim: 0 };

    for cmd in &commands {
        sub.command(cmd);
    }

    println!("{:?}", sub);
    println!("{}", sub.horiz * sub.vert);

    Ok(())
}
