use std::collections::{HashSet, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn neighbour(map: &mut Vec<Vec<i64>>, frontier: &mut VecDeque<(usize, usize)>, x: i64, y: i64) {
    if y >= 0 && y < map.len() as i64 && x >= 0 && x < map[0].len() as i64 {
        frontier.push_back((x as usize, y as usize));
    }
}

fn flash(map: &mut Vec<Vec<i64>>, flashed: &mut HashSet<(usize, usize)>, x: usize, y: usize) {
    let mut frontier: VecDeque<(usize, usize)> = VecDeque::new();

    frontier.push_back((x, y));

    while !frontier.is_empty() {
        let (cx, cy) = frontier.pop_front().unwrap();
        if flashed.contains(&(cx, cy)) {
            continue;
        }

        map[cy][cx] += 1;

        if map[cy][cx] > 9 {
            flashed.insert((cx, cy));
            map[cy][cx] = 0;

            neighbour(map, &mut frontier, cx as i64 - 1, cy as i64 - 1);
            neighbour(map, &mut frontier, cx as i64 + 0, cy as i64 - 1);
            neighbour(map, &mut frontier, cx as i64 + 1, cy as i64 - 1);
            neighbour(map, &mut frontier, cx as i64 - 1, cy as i64 + 0);
            neighbour(map, &mut frontier, cx as i64 + 1, cy as i64 + 0);
            neighbour(map, &mut frontier, cx as i64 - 1, cy as i64 + 1);
            neighbour(map, &mut frontier, cx as i64 + 0, cy as i64 + 1);
            neighbour(map, &mut frontier, cx as i64 + 1, cy as i64 + 1);
        }
    }
}

fn step(map: &mut Vec<Vec<i64>>) -> usize {
    let height = map.len();
    let width = map[0].len();
    let mut flashed: HashSet<(usize, usize)> = HashSet::new();

    for y in 0..height {
        for x in 0..width {
            map[y][x] += 1;
        }
    }

    for y in 0..height {
        for x in 0..width {
            if map[y][x] > 9 {
                flash(map, &mut flashed, x, y);
            }
        }
    }

    flashed.len()
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut map: Vec<Vec<i64>> = reader.lines().map(|l| l.unwrap().trim().chars().map(|x| x as i64 - '0' as i64).collect()).collect();

    //// part1
    //let flashes: Vec<usize> = (1..=100).map(|_| step(&mut map)).scan(0, |acc, x| {
    //    *acc += x;
    //    Some(*acc)
    //}).collect();
    //println!("{:?}", flashes);

    let mut last_flash = 0;
    let mut i = 0;

    while last_flash < 100 {
        i += 1;
        last_flash = step(&mut map);
    }

    println!("{}", i);

    Ok(())
}
