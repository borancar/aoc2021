use std::hash::{Hash, Hasher};
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
enum Typ {
    A,
    B,
    C,
    D
}

#[derive(Copy, Clone, Debug, Hash, PartialEq)]
struct Amphipod {
    typ: Typ,
    x: i64,
    y: i64,
    consume: i64,
    final_position: bool,
    moved: bool,
}

#[derive(Clone, Debug)]
struct State(Vec<Amphipod>);

impl Hash for State {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for a in self.0.iter() {
            a.hash(state);
        }
    }
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        for (a1, a2) in self.0.iter().zip(other.0.iter()) {
            if !a1.eq(a2) { return false; }
        }

        true
    }
}

impl Eq for State {}

fn is_solution(amphipods: &Vec<Amphipod>) -> bool {
    amphipods.iter().all(|a| {
        match a.typ {
            Typ::A => a.x == 3 && a.y > 1,
            Typ::B => a.x == 5 && a.y > 1,
            Typ::C => a.x == 7 && a.y > 1,
            Typ::D => a.x == 9 && a.y > 1,
        }
    })
}

fn destination_x(a: &Amphipod) -> i64 {
    match a.typ {
        Typ::A => 3,
        Typ::B => 5,
        Typ::C => 7,
        Typ::D => 9,
    }
}

fn can_move(a: &Amphipod, amphipods: &Vec<Amphipod>, dx: i64, dy: i64) -> bool {
    if amphipods.iter().any(|a2| a2.x == dx && a2.y == dy) { return false; }

    for y in 1..a.y {
        if amphipods.iter().any(|a2| a2.x == a.x && a2.y == y) { return false; }
    }

    if a.x < dx {
        for x in (a.x+1)..=dx {
            if amphipods.iter().any(|a2| a2.x == x && a2.y == 1) { return false; }
        }
    } else if a.x > dx {
        for x in dx..a.x {
            if amphipods.iter().any(|a2| a2.x == x && a2.y == 1) { return false; }
        }
    }

    for y in 2..=dy {
        if amphipods.iter().any(|a2| a2.x == dx && a2.y == y) { return false; }
    }

    true
}

fn correct_in_room(amphipods: &Vec<Amphipod>, x: i64) -> usize {
    amphipods.iter().filter(|a| a.x == x && destination_x(a) == x).count()
}

fn move_amphipod(a: &Amphipod, dx: i64, dy: i64) -> (Amphipod, i64) {
    let energy = energy_to_move(a, dx, dy);

    (Amphipod {
        typ: a.typ,
        x: dx,
        y: dy,
        moved: true,
        final_position: dy > 1,
        consume: a.consume,
    }, energy)
}

fn energy_to_move(a: &Amphipod, dx: i64, dy: i64) -> i64 {
    ((dy-1) + (a.y-1) + (dx-a.x).abs()) * a.consume
}

fn print_candidate(amphipods: &Vec<Amphipod>) {
    let mut playfield = [ ['#','#','#','#','#','#','#','#','#','#','#','#','#']
                        , ['#','.','.','.','.','.','.','.','.','.','.','.','#']
                        , ['#','#','#','.','#','.','#','.','#','.','#','#','#']
                        , [' ',' ','#','.','#','.','#','.','#','.','#',' ',' ']
                        , [' ',' ','#','.','#','.','#','.','#','.','#',' ',' ']
                        , [' ',' ','#','.','#','.','#','.','#','.','#',' ',' ']
                        , [' ',' ','#','#','#','#','#','#','#','#','#',' ',' ']
                        ];

    for a in amphipods {
        playfield[a.y as usize][a.x as usize] = match a.typ {
            Typ::A => 'A',
            Typ::B => 'B',
            Typ::C => 'C',
            Typ::D => 'D',
        };
    }

    for line in playfield {
        for c in line {
            print!("{}", c);
        }
        println!("");
    }
}

fn estimate(amphipods: &Vec<Amphipod>) -> i64 {
    amphipods.iter().map(|a| if a.final_position { 0 } else { energy_to_move(a, destination_x(a), 2) }).sum()
}

fn solve_r(memo: &mut HashMap<State, i64>, amphipods: &mut Vec<Amphipod>, max_y: usize) -> i64 {
    if is_solution(amphipods) {
        return 0;
    }

    if memo.contains_key(&State(amphipods.clone())) {
        return memo[&State(amphipods.clone())];
    }

    let mut min_energy = std::i64::MAX;
    let mut put_room = false;

    'room: for i in 0..amphipods.len() {
        let dx = destination_x(&amphipods[i]);
        let correct = correct_in_room(&amphipods, dx);

        if amphipods[i].final_position {
            continue;
        }

        if amphipods[i].x == destination_x(&amphipods[i]) && amphipods[i].y == max_y as i64 {
            continue;
        }

        let orig = amphipods[i].clone();

        for dy in (2..=max_y).rev() {
            if correct == max_y - dy && can_move(&orig, &amphipods, dx, dy as i64) {
                let moved_amphipod = move_amphipod(&orig, dx, dy as i64);
                amphipods[i] = moved_amphipod.0;
                if moved_amphipod.1 + estimate(amphipods) < min_energy {
                    let next_steps = solve_r(memo, amphipods, max_y);
                    if next_steps != std::i64::MAX && moved_amphipod.1 + next_steps < min_energy {
                        min_energy = moved_amphipod.1 + next_steps;
                    }
                }
                amphipods[i] = orig;
                put_room = true;
                break 'room;
            }
        }
    }

    if !put_room {
        for i in 0..amphipods.len() {
            let orig = amphipods[i].clone();

            if !amphipods[i].moved {
                for tx in [11,1,10,2,4,6,8] {
                    if can_move(&orig, &amphipods, tx, 1) {
                        let moved_amphipod = move_amphipod(&orig, tx, 1);
                        amphipods[i] = moved_amphipod.0;
                        if moved_amphipod.1 + estimate(amphipods) < min_energy {
                            let next_steps = solve_r(memo, amphipods, max_y);
                            if next_steps != std::i64::MAX && moved_amphipod.1 + next_steps < min_energy {
                                min_energy = moved_amphipod.1 + next_steps;
                            }
                        }
                        amphipods[i] = orig;
                    }
                }
            }

            amphipods[i] = orig;
        }
    }

    memo.insert(State(amphipods.clone()), min_energy);

    min_energy
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut amphipods: Vec<Amphipod> = Vec::new();

    let mut max_y = 0;

    for (y, line) in reader.lines().enumerate() {
        for (x, c) in line.unwrap().chars().enumerate() {
            match c {
                'A' | 'B' | 'C' | 'D' => {
                    max_y = std::cmp::max(y, max_y);
                    amphipods.push(Amphipod{
                        typ: match c {
                            'A' => Typ::A,
                            'B' => Typ::B,
                            'C' => Typ::C,
                            'D' => Typ::D,
                            _ => panic!("Unexpected type {}", c),
                        },
                        x: x as i64,
                        y: y as i64,
                        consume: match c {
                            'A' => 1,
                            'B' => 10,
                            'C' => 100,
                            'D' => 1000,
                            _ => panic!("Unexpected type {}", c),
                        },
                        final_position: false,
                        moved: false,
                    });
                }
                _ => {},
            }
        }
    }

    let mut memo: HashMap<State, i64> = HashMap::new();

    println!("{}", solve_r(&mut memo, &mut amphipods, max_y));

    Ok(())
}
