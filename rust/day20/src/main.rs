use std::fs::File;
use std::io::{BufRead, BufReader};

fn get3x3(map: &Vec<Vec<char>>, x: i64, y: i64, blank: char) -> i64 {
    let mut mat3x3 = [[blank; 3]; 3];

    for dy in -1..=1 {
        for dx in -1..=1 {
            if x + dx >= 0 && x + dx < map[0].len() as i64 &&
                y + dy >= 0 && y + dy < map.len() as i64  {
                    mat3x3[(dy+1) as usize][(dx+1) as usize] =
                        map[(y+dy) as usize][(x+dx) as usize];
                }
        }
    }

    mat3x3.iter().flat_map(|r| {
        r.iter().map(|x|
            match x {
                '#' => 1,
                '.' => 0,
                c => panic!("Invalid {}", c),
            }
        ).collect::<Vec<i64>>()
    }).fold(0, |acc, x| 2*acc + x)
}

fn apply(map: &Vec<Vec<char>>, algo: &Vec<char>, i: usize) -> Vec<Vec<char>> {
    let mut result: Vec<Vec<char>> = Vec::new();

    let blank = if i % 2 == 0 { '.' } else { algo[0] };

    for y in -1..(map.len() as i64 + 1) {
        let mut new_row: Vec<char> = Vec::new();

        for x in -1..(map[0].len() as i64 + 1) {
            new_row.push(algo[get3x3(map, x as i64, y as i64, blank) as usize]);
        }

        result.push(new_row);
    }

    result
}

fn print_map(map: &Vec<Vec<char>>) {
    for row in map {
        for c in row {
            print!("{}", c);
        }
        println!("");
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut lines = reader.lines();

    let algo: Vec<char> = (&mut lines).take(1).flat_map(|l| {
        let aline = l.unwrap();
        let trimmed = aline.trim();

        trimmed.chars().collect::<Vec<char>>()
    }).collect();

    let mut map: Vec<Vec<char>> = lines.skip(1).map(|l| {
        let aline = l.unwrap();
        let trimmed = aline.trim();

        trimmed.chars().collect::<Vec<char>>()
    }).collect();

    for i in 0..50 {
        map = apply(&map, &algo, i);
    }

    let lit = map.iter().map(|r| r.iter().filter(|&&v| v == '#').count()).sum::<usize>();

    println!("{}", lit);

    Ok(())
}
