use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
enum Command {
    Insert(usize, usize),
    FoldX(usize),
    FoldY(usize),
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut endx = 1000;
    let mut endy = 1000;

    let cmds: Vec<Command> = reader.lines().map(|l| {
        l.unwrap().trim().to_string()
    }).filter(|l| { l.len() > 0
    }).map(|l| {
        match l {
            n if n.starts_with("fold along x") => {
                let parts: Vec<&str> = n.split("=").collect();
                let x = parts[1].parse::<usize>().unwrap();
                endx = std::cmp::min(endx, x);
                Command::FoldX(x)
            },
            n if n.starts_with("fold along y") => {
                let parts: Vec<&str> = n.split("=").collect();
                let y = parts[1].parse::<usize>().unwrap();
                endy = std::cmp::min(endy, y);
                Command::FoldY(y)
            },
            _ => {
                let parts: Vec<&str> = l.split(",").collect();
                let x = parts[0].parse::<usize>().unwrap();
                let y = parts[1].parse::<usize>().unwrap();
                Command::Insert(x, y)
            },
        }
    }).collect();

    let mut points: HashSet<(usize, usize)> = HashSet::new();

    for cmd in cmds {
        match cmd {
            Command::Insert(x, y) => {
                points.insert((x, y));
            },
            Command::FoldX(fx) => {
                points = points.iter().map(|&(x, y)| if x > fx { (2*fx - x, y) } else { (x, y) }).collect();
            },
            Command::FoldY(fy) => {

                points = points.iter().map(|&(x, y)| if y > fy { (x, 2*fy - y) } else { (x, y) }).collect();
            }
        }
    }

    let mut folded: Vec<Vec<char>> = Vec::new();

    for _y in 0..=endy {
        let mut row = Vec::new();
        row.resize(endx as usize, '.');
        folded.push(row);
    }

    for &(x, y) in &points {
        folded[y][x] = '#';
    }

    for row in folded {
        for c in row {
            print!("{}", c);
        }
        println!("");
    }

    Ok(())
}
