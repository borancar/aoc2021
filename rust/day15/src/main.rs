use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct State {
    risk: i64,
    x: usize,
    y: usize,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.risk.cmp(&self.risk)
            .then_with(|| self.x.cmp(&other.x))
            .then_with(|| self.y.cmp(&other.y))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn try_next(heap: &mut BinaryHeap<State>, risk: i64, map: &[[i64; 500]; 500], x: i64, y: i64, endx: i64, endy: i64) {
    if x >= 0 && x <= endx && y >= 0 && y <= endy {
        heap.push(State{
            risk: risk + map[y as usize][x as usize],
            x: x as usize,
            y: y as usize,
        })
    }
}

fn lowest_risk(map: &[[i64; 500]; 500], endx: usize, endy: usize) -> i64 {
    let start = State{
        risk: 0,
        x: 0,
        y: 0,
    };

    let mut heap: BinaryHeap<State> = BinaryHeap::new();
    let mut visited: HashSet<(usize, usize)> = HashSet::new();
    heap.push(start);

    while !heap.is_empty() {
        let State{risk, x, y} = heap.pop().unwrap();

        if visited.contains(&(x, y)) {
            continue;
        }

        visited.insert((x, y));

        if x == endx && y == endy {
            return risk;
        }

        try_next(&mut heap, risk, map, x as i64 - 1, y as i64 + 0, endx as i64, endy as i64);
        try_next(&mut heap, risk, map, x as i64 + 0, y as i64 - 1, endx as i64, endy as i64);
        try_next(&mut heap, risk, map, x as i64 + 1, y as i64 + 0, endx as i64, endy as i64);
        try_next(&mut heap, risk, map, x as i64 + 0, y as i64 + 1, endx as i64, endy as i64);
    }

    0
}

fn wrap9(x: i64) -> i64 {
    if x > 9 { x - 9 } else { x }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);

    let mut map = [[0i64; 500]; 500];

    let mut base_size = 0;

    for (y, l) in reader.lines().enumerate() {
        let aline = l.unwrap();
        let trimmed = aline.trim();

        let base: Vec<i64> = trimmed.chars().map(|c| c as i64 - '0' as i64).collect();
        base_size = base.len();

        for (x, &r) in base.iter().enumerate() {
            map[y][x] = r;

            for my in 0..5 {
                for mx in 0..5 {
                    if mx == 0 && my == 0 {
                        continue;
                    }

                    map[y+my*base_size][x+mx*base_size] = wrap9(r + mx as i64 + my as i64);
                }
            }
        }
    }

    println!("{}", lowest_risk(&map, base_size * 5 - 1, base_size * 5 - 1));

    Ok(())
}
