use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct Node {
    elem: char,
    next: Option<Box<Node>>,
}

struct NodeIter<'a> {
    current: Option<&'a Node>,
}

impl Iterator for NodeIter<'_> {
    type Item = char;

    fn next(&mut self) -> Option<char> {
        self.current.take().map(|a| {
            self.current = a.next.as_deref();
            a.elem
        })
    }
}

fn iter_list(node: &Node) -> NodeIter {
    NodeIter { current: Some(node) }
}

fn insert_after(node: &mut Node, c: char) -> &mut Box<Node> {
    let new = Box::new(Node{
        elem: c,
        next: node.next.take(),
    });

    node.next = Some(new);

    node.next.as_mut().unwrap()
}

fn step_list(mut node: &mut Node, inserts: &HashMap<String, char>) {
    while let Some(next) = &node.next {
        let mut pair = String::new();
        pair.push(node.elem);
        pair.push(next.elem);
        if inserts.contains_key(&pair) {
            insert_after(node, inserts[&pair]);
            node = node.next.as_mut().unwrap();
        }
        node = node.next.as_mut().unwrap();
    }
}

fn chars_to_nodelist(chars: &Vec<char>) -> Node {
    let mut template = Node {
        elem: chars[0],
        next: None,
    };

    let mut last = &mut template;
    for &c in &chars[1..] {
        last = insert_after(last, c);
    }

    template
}

fn chars_to_pairsf(chars: &Vec<char>) -> HashMap<String, i64> {
    let mut pairs: HashMap<String, i64> = HashMap::new();

    for p in chars.windows(2) {
        let pair = String::from_iter(p.iter());

        let update = pairs.get(&pair).unwrap_or(&0) + 1;
        pairs.insert(pair, update);
    }

    pairs
}

fn step_pairs(pairs: &HashMap<String, i64>, inserts: &HashMap<String, char>) -> HashMap<String, i64> {
    let mut new_pairs: HashMap<String, i64> = HashMap::new();

    for (p, f) in pairs {
        if inserts.contains_key(p) {
            let c1 = p.chars().next().unwrap();
            let c2 = inserts[p];
            let c3 = p.chars().skip(1).next().unwrap();

            let np1 = String::from_iter([c1, c2].iter());
            let np2 = String::from_iter([c2, c3].iter());

            let update_np1 = new_pairs.get(&np1).unwrap_or(&0) + *f;
            new_pairs.insert(np1, update_np1);

            let update_np2 = new_pairs.get(&np2).unwrap_or(&0) + *f;
            new_pairs.insert(np2, update_np2);
        } else {
            new_pairs.insert(p.to_string(), *f);
        }
    }

    new_pairs
}

fn occurs_from_list(node: &Node) -> HashMap<char, i64> {
    let mut occurs: HashMap<char, i64> = HashMap::new();

    for c in iter_list(node) {
        occurs.insert(c, occurs.get(&c).unwrap_or(&0) + 1);
    }

    occurs
}

fn occurs_from_pairs(pairs: &HashMap<String, i64>) -> HashMap<char, i64> {
    let mut occurs: HashMap<char, i64> = HashMap::new();

    for (p, f) in pairs {
        let c1 = p.chars().next().unwrap();
        let c2 = p.chars().skip(1).next().unwrap();

        occurs.insert(c1, occurs.get(&c1).unwrap_or(&0) + f);
        occurs.insert(c2, occurs.get(&c2).unwrap_or(&0) + f);
    }

    occurs
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);

    let mut template = String::new();
    reader.read_line(&mut template)?;
    template = template.trim().to_string();

    let chars: Vec<char> = template.chars().collect();

    let mut inserts: HashMap<String, char> = HashMap::new();

    for line in reader.lines().skip(1) {
        let aline = line.unwrap();
        let parts: Vec<&str> = aline.trim().split("->").collect();

        inserts.insert(parts[0].trim().to_string(), parts[1].trim().to_string().chars().next().unwrap());
    }

    let mut nodelist = chars_to_nodelist(&chars);

    for _i in 0..10 {
        step_list(&mut nodelist, &inserts);
    }

    let occurs1 = occurs_from_list(&nodelist);

    let (_, min_occur) = occurs1.iter().min_by_key(|(_, &v)| v).unwrap();
    let (_, max_occur) = occurs1.iter().max_by_key(|(_, &v)| v).unwrap();

    println!("{}", max_occur - min_occur);

    let mut pairs = chars_to_pairsf(&chars);

    for _i in 0..40 {
        pairs = step_pairs(&pairs, &inserts);
    }

    let occurs2 = occurs_from_pairs(&pairs);

    let (_, min_occur) = occurs2.iter().min_by_key(|(_, &v)| v).unwrap();
    let (_, max_occur) = occurs2.iter().max_by_key(|(_, &v)| v).unwrap();

    println!("{}", (max_occur - min_occur + 1)/2);

    Ok(())
}
